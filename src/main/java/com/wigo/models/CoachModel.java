package com.wigo.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity(name = "coach")
@NoArgsConstructor
@Getter
@Setter
public class CoachModel {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "coach_id_sequence",
            sequenceName = "coach_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "coach_id_sequence"
    )
    @Column(name = "id")
    private Integer id;
    private String name;
    private String phone;
    private String image;
    private String birthday;
    private String gender;
    private Integer gym;
    private Boolean coachOfTheMonth;
    private Integer upVotes;
    private Integer downVotes;
    private String location;
    private String description;
    private String nationality;
    private String level;
    private String createdAt;
    private String modifiedAt;
    private String workoutTypes;

}
