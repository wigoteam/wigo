
package com.wigo.controllers;


import com.wigo.Helpers.RequestHolder;
import com.wigo.models.ErrorResponseType;
import com.wigo.models.ProductCategoryModel;
import com.wigo.models.ResponseModel;
import com.wigo.models.VotingModel;
import com.wigo.repositories.VotingRepository;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController

public class VotingController {
    private final VotingRepository votingRepository;






    ResponseModel response = new ResponseModel();

    public VotingController(VotingRepository votingRepository) {
        this.votingRepository = votingRepository;
    }





    public List<VotingModel> getAllVotes() {
        return votingRepository.findAll();
    }



    public ResponseModel createVote( RequestHolder.VoteRequest request )  {

        Optional<VotingModel> votingModel=votingRepository.findByUserId(request.userId());
        if(votingModel.isEmpty()){
            VotingModel model = new VotingModel();
            model.setCreatedAt(LocalDate.now().toString());
            model.setVotedId(request.votedId());
            model.setUserId(request.userId());
            model.setModifiedAt(LocalDate.now().toString());
            votingRepository.save(model);
        }else {
            VotingModel model=votingModel.get();
            model.setModifiedAt(LocalDate.now().toString());
            model.setVotedId(request.votedId());
            votingRepository.save(model);
        }
        response.setMessage("voted Successfully");
        response.setErrorType(ErrorResponseType.Nothing);
        response.setErrorCode("20000");
        response.setObject(null);
        response.setReturnedBoolean(true);
        response.setThereIsAnError(false);
        response.setReturnedInteger(null);
        response.setReturnedList(null);
        response.setReturnedString(null);
        response.setReturnedMultipartFile(null);
        response.setThereIsAnError(false);
        return response;
    }


}

