package com.wigo.models;

import com.wigo.AppConstants;
import jakarta.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class ApplicationMainDataChecker {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "check_data__id_sequence",
            sequenceName = "check_data_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "check_data_id_sequence"
    )
    @Column(name = "id")
    private Integer id;
    private String dataBaseUpdatedAt;
    private String applicationVersion= AppConstants.APPLICATION_VERSION;
    private String applicationLog;
    private String applicationDevVersion=AppConstants.APPLICATION_DEV_VERSION;

}
