package com.wigo.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@Getter
@Setter
@Entity(name = "notification")
public class NotificationModel {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "notification_id_sequence",
            sequenceName = "notification_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "notification_id_sequence"
    )

    @Column(name = "id")
    private Integer id;
    private String type;
    private String title;
    private String about;
    private String category;
    private String mentionName;
    private String sender;
    private String message;
    private String imageUrl;
    private String color;
    private String filter;
    private String createdAt;

    @Override
    public String toString() {
        return "NotificationModel{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", title='" + title + '\'' +
                ", about='" + about + '\'' +
                ", category='" + category + '\'' +
                ", mentionName='" + mentionName + '\'' +
                ", sender='" + sender + '\'' +
                ", message='" + message + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", color='" + color + '\'' +
                ", filter='" + filter + '\'' +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }
}
