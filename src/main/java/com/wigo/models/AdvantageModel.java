package com.wigo.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdvantageModel {
    private String title;
    private String description;

    @Override
    public String toString() {
        return "{" +
                "\"title\":\"" + title + "\"" +
                ",\" description\":\"" + description + "\"" +
                '}';
    }
}
