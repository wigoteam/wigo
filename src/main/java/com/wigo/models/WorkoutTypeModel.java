package com.wigo.models;

import jakarta.persistence.*;


@Entity(name = "workoutType")
public class WorkoutTypeModel {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "workout_type_id_sequence",
            sequenceName = "workout_type_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "workout_type_id_sequence"
    )
    @Column(name = "id")
    private Integer id;

    private String WorkoutTypeName;

    private String WorkoutTypeColor;
    private String createdAt;
    private String modifiedAt;

    public WorkoutTypeModel(Integer id, String workoutTypeName, String workoutTypeColor, String createdAt, String modifiedAt) {
        this.id = id;
        WorkoutTypeName = workoutTypeName;
        WorkoutTypeColor = workoutTypeColor;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public WorkoutTypeModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWorkoutTypeName() {
        return WorkoutTypeName;
    }

    public void setWorkoutTypeName(String workoutTypeName) {
        WorkoutTypeName = workoutTypeName;
    }

    public String getWorkoutTypeColor() {
        return WorkoutTypeColor;
    }

    public void setWorkoutTypeColor(String workoutTypeColor) {
        WorkoutTypeColor = workoutTypeColor;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }
}
