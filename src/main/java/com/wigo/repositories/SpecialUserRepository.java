package com.wigo.repositories;

import com.wigo.models.SpecialUserModel;
import com.wigo.models.UserProfileModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SpecialUserRepository extends JpaRepository<SpecialUserModel,Integer> {
    Optional<SpecialUserModel> findByMail(Object email);
    Optional<SpecialUserModel> findByOwnedId(String ownedId);
}
