package com.wigo.controllers;

import com.wigo.Helpers.RequestHolder;
import com.wigo.Utils;
import com.wigo.configs.JwtService;
import com.wigo.models.AuthenticatedUser;
import com.wigo.models.ErrorResponseType;
import com.wigo.models.ResponseModel;
import com.wigo.models.UserProfileModel;
import com.wigo.repositories.AuthenticatedUserRepository;
import com.wigo.repositories.UserProfileRepository;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController

public class UserProfileAuthenticationController {
    private final UserProfileRepository userProfileRepository;
    private final AuthenticatedUserRepository authenticatedUserRepository;
    private final JwtService jwtService;
    public UserProfileAuthenticationController(UserProfileRepository userProfileRepository, AuthenticatedUserRepository authenticatedUserRepository, JwtService jwtService) {
        this.userProfileRepository = userProfileRepository;
        this.authenticatedUserRepository = authenticatedUserRepository;
        this.jwtService = jwtService;
    }


    public ResponseModel getUserProfileById( Integer id) {
        Optional<UserProfileModel> model=userProfileRepository.findById(id);
        if(model.isPresent()){
            response.setMessage("Exist");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model.get());
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
        }else {
            response.setMessage("unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
        }
        response.setReturnedInteger(null);
        response.setReturnedList(null);
        response.setReturnedString(null);
        response.setReturnedMultipartFile(null);
        return response;
    }


    public ResponseModel loginNormalUser( RequestHolder.LoginRequest request) {
        System.out.println(request);
        Optional<UserProfileModel> profileModel = userProfileRepository.findByMail(request.email());
        if (profileModel.isPresent()) {
            System.out.println(profileModel.get());
            String password=profileModel.get().getPassword();
            System.out.println("password = "+password);
            if ((password.trim()).equals(request.password().trim())) {
                String token= jwtService.generateToken(
                        profileModel.get().getMail(),
                        profileModel.get().getFirstName()+" "+profileModel.get().getLastName(),
                        profileModel.get().getPhone(),
                        profileModel.get().getSerialId()
                );
                profileModel.get().setToken(token);
                newConnexion(new  RequestHolder.NewConnexionRequest(
                        profileModel.get().getId(),
                        token,
                        profileModel.get().getMail()));
                userProfileRepository.save(profileModel.get());
                response.setErrorType(ErrorResponseType.Nothing);
                response.setMessage("user logged In");
                response.setErrorCode("20000");
                response.setThereIsAnError(false);
                response.setObject(profileModel.get());
            } else {
                response.setErrorType(ErrorResponseType.WrongPassword);
                response.setMessage("wrong password");
                response.setErrorCode("40000");
                response.setThereIsAnError(true);
                response.setObject(null);
            }
        } else {
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setMessage("no user found with this information");
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setObject(null);
        }

        return response;


    }


    public ResponseModel preLogin(RequestHolder.PreLoginRequest request) {
        Optional<UserProfileModel> profileModel = userProfileRepository.findByMail(request.email());
        if (profileModel.isPresent()) {
                response.setErrorType(ErrorResponseType.Nothing);
                response.setReturnedString("");
                response.setMessage("Valid");
                response.setErrorCode("20000");
                response.setThereIsAnError(false);
                response.setObject(null);
        } else {
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedString(null);
            response.setMessage("no user found with this information");
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setObject(null);
        }
        return response;
    }



    public ResponseModel checkUserExistByEmail(String email) {
        Optional<UserProfileModel> profileModel = userProfileRepository.findByMail(email);
        if (profileModel.isPresent()) {
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setMessage("true");
            response.setObject(profileModel.get());
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);

        } else {
            response.setErrorType(ErrorResponseType.Nothing);
            response.setMessage("false");
            response.setReturnedBoolean(false);
            response.setErrorCode("40000");
            response.setThereIsAnError(false);
            response.setObject(null);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);

        }
        return response;


    }




    public List<UserProfileModel> getAllUsersProfile() {
        return userProfileRepository.findAll();
    }



    ResponseModel response = new ResponseModel();






    public ResponseModel createUserProfile(RequestHolder.NewUserProfileRequest request) {
        if (!checkUserExistByEmail(request.mail()).getReturnedBoolean()) {
            UserProfileModel profile = new UserProfileModel();
            profile.setBirthday(request.birthday());
            profile.setChats(request.chats());
            profile.setPassword(request.password());
            profile.setGender(request.gender());
            profile.setImage(request.image());
            profile.setFirstName(request.firstName());
            profile.setGallery(request.gallery());
            profile.setGym(request.gym());
            profile.setHeight(request.height());
            profile.setLastName(request.lastName());
            profile.setMail(request.mail());
            profile.setNationality(request.nationality());
            profile.setWeight(request.weight());
            Date now=new Date(System.currentTimeMillis());
            profile.setPro(request.pro());
            profile.setPhone(request.phone());
            profile.setPhoneType(request.phoneType());
            String serialId=Utils.generateSerialKey();
            profile.setCreatedAt(LocalDate.now().toString());
            profile.setModifiedAt(LocalDate.now().toString());

            profile.setSerialId(serialId);
            String token= jwtService.generateToken(
                     request.mail(),
                    request.firstName()+" "+request.lastName(),
                     request.phone(),
                     serialId
            );
            profile.setToken(token);
          newConnexion(new  RequestHolder.NewConnexionRequest(
                    profile.getId(),
                    token,
                    request.mail()));
            userProfileRepository.save(profile);

            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(profile);
            response.setMessage("user created successfully");
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(profile.getId());
            response.setReturnedList(null);
            response.setReturnedString(profile.getToken());
            response.setReturnedMultipartFile(null);
        } else {
            response.setErrorType(ErrorResponseType.CorruptedData);
            response.setMessage("user already exist");
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        return response;
    }


    public void newConnexion(RequestHolder.NewConnexionRequest request){
        Optional<AuthenticatedUser> user=authenticatedUserRepository.findByUserId(request.userId());
        user.ifPresent(authenticatedUser -> authenticatedUserRepository.deleteById(authenticatedUser.getId()));
        AuthenticatedUser authenticatedUser=new AuthenticatedUser();
        authenticatedUser.setUserId(request.userId());
        authenticatedUser.setToken(request.token());
        authenticatedUser.setUserMail(request.mail());
        authenticatedUser.setLastSignInDate(LocalDate.now().toString());
        authenticatedUserRepository.save(authenticatedUser);
    }
    public ResponseModel userIsConnected(Integer id){
        Optional<AuthenticatedUser> user=authenticatedUserRepository.findByUserId(id);
       if (user.isPresent()){
           response.setErrorType(ErrorResponseType.Nothing);
           response.setMessage("user logged in");
           response.setReturnedBoolean(true);
           response.setObject(null);
           response.setErrorCode("20000");
           response.setThereIsAnError(false);
           response.setReturnedInteger(null);
           response.setReturnedList(null);
           response.setReturnedString(null);
           response.setReturnedMultipartFile(null);
       }else {
           response.setErrorType(ErrorResponseType.CorruptedData);
           response.setMessage("user need to login again");
           response.setReturnedBoolean(false);
           response.setObject(null);
           response.setErrorCode("40000");
           response.setThereIsAnError(true);
           response.setReturnedInteger(null);
           response.setReturnedList(null);
           response.setReturnedString(null);
           response.setReturnedMultipartFile(null);
       }
       return response;
         }

    public Boolean tokenIsValid(Integer userId){
        Optional<AuthenticatedUser> user=authenticatedUserRepository.findByUserId(userId);
        return user.isPresent();
    }


    public ResponseModel updateUserProfile( RequestHolder.UpdateUserProfile request) {
        Optional<UserProfileModel> profileModel = userProfileRepository.findById(request.id());
        if (profileModel.isPresent()) {
            UserProfileModel profile = profileModel.get();
            profile.setBirthday(request.birthday());
            profile.setChats(request.chats());
            profile.setGender(request.gender());
            profile.setImage(request.image());
            profile.setFirstName(request.firstName());
            profile.setGallery(request.gallery());
            profile.setGym(request.gym());
            profile.setHeight(request.height());
            profile.setLastName(request.lastName());
            profile.setMail(request.mail());
            profile.setNationality(request.nationality());
            profile.setWeight(request.weight());
            profile.setPro(request.pro());
            profile.setPhone(request.phone());
            profile.setPhoneType(request.phoneType());
            profile.setCreatedAt(LocalDate.now().toString());
            profile.setModifiedAt(LocalDate.now().toString());
            userProfileRepository.save(profile);
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(profile);
            response.setMessage("user updated successfully");
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(profile.getId());
            response.setReturnedList(null);
            response.setReturnedString(profile.getToken());
            response.setReturnedMultipartFile(null);
        } else {
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setMessage("user unfounded");
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        return response;
    }

    public ResponseModel deleteUserProfile(Integer id) {
        if (userProfileRepository.existsById(id)) {
            userProfileRepository.deleteById(id);
            response.setMessage("user deleted Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setMessage("user unfounded with this id");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        return response;
    }


}
