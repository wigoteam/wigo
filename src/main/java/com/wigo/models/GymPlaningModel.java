package com.wigo.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GymPlaningModel {
    private String planingDay;
    private List<GymDailyEventModel> listOfGymEvents;

    @Override
    public String toString() {
        return "{" +
                "\"planingDay\":\"" + planingDay + "\"" +
                ",\"listOfGymEvents\":" + listOfGymEvents +
                "}";
    }
}
