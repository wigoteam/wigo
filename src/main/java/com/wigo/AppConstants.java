package com.wigo;



public class AppConstants {
    public static final String API_V1 = "/v1";
    public static final String APPLICATION_VERSION = "0.0.1";
    public static final String APPLICATION_DEV_VERSION = "1.0.0";
    public static final String API_MAIN_BASE_URL ="/api";
    public static final String API_BASE_URL = API_MAIN_BASE_URL+API_V1;
    public static final String TOKEN_KEY="0ebc6eba746722330f07ae70930e057b3a5a82f850fad47d16dcb1056175aec9";
    public static final long TOKEN_KEY_EXPIRATION=86400000;
    public static final long TOKEN_KEY_REFRESH_EXPIRATION=604800000;
    private AppConstants() {

    }

}