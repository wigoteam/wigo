package com.wigo.repositories;

import com.wigo.models.CoachModel;
import com.wigo.models.GymModel;
import com.wigo.models.UserProfileModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CoachRepository extends JpaRepository<CoachModel,Integer> {

    Optional<CoachModel> findByName(String name);
    Optional<CoachModel> findByPhone(String name);
    List<CoachModel>     findByGym(Integer gym);
}
