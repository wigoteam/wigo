package com.wigo.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@Entity(name = "specialUser")

public class SpecialUserModel  {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "special_user_id_sequence",
            sequenceName = "special_user_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "special_user_id_sequence"
    )
    @Column(name = "id")
    private Integer id;
    private String mail;
    private String password;
    @Enumerated(EnumType.STRING)
    private Role role;
    private String ownedId;
    private String createdAt;
    private String modifiedAt;

    public SpecialUserModel(Integer id, String mail, String password, Role role, String ownedId, String createdAt, String modifiedAt) {
        this.id = id;
        this.mail = mail;
        this.password = password;
        this.role = role;
        this.ownedId = ownedId;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }
}


