package com.wigo.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@Entity(name = "reminder")
public class ReminderModel {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "notification_id_sequence",
            sequenceName = "notification_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "notification_id_sequence"
    )

    @Column(name = "id")
    private Integer id;
    private String header;
    private String title;
    private String message;
    private String gyms;
    private Boolean special;
    private Boolean isReminder;
    private String reminderDate;
    private String createdAt;
    private String modifiedAt;

    public ReminderModel(Integer id, String header, String title, String message, String gyms, Boolean special, Boolean isReminder, String reminderDate, String createdAt, String modifiedAt) {
        this.id = id;
        this.header = header;
        this.title = title;
        this.message = message;
        this.gyms = gyms;
        this.special = special;
        this.isReminder = isReminder;
        this.reminderDate = reminderDate;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }
}
