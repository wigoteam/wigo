package com.wigo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utils {

 public  static String generateSerialKey(){

        Random random = new Random();

         return  random.ints(48, 123)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(250)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

    }

    public static String generateSerialId() {

        Random random = new Random();

        return "@"+random.ints(48, 57)
                .limit(100)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();




    }
    public  static List<String> returnListFromString(String tableInString) {
        List<String> result = new ArrayList<>();
        if (!tableInString.isBlank()) {
            List<String> holder = List.of((tableInString.split("---")));
            result.addAll(holder);
            result.removeIf(String::isBlank);


            return result;
        } else {
            return new ArrayList<>();
        }
    }
}
