
package com.wigo.controllers;


import com.wigo.Helpers.RequestHolder;
import com.wigo.models.ErrorResponseType;
import com.wigo.models.NotificationModel;
import com.wigo.models.ResponseModel;
import com.wigo.repositories.NotificationRepository;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController

public class NotificationController {
    private final NotificationRepository repository;


    public NotificationController(NotificationRepository repository) {
        this.repository = repository;

    }

    ResponseModel response = new ResponseModel();















    public List<NotificationModel> getAllNotification() {
        return repository.findAll();
    }




    public ResponseModel createNotification( RequestHolder.NewNotificationRequest request )  {

System.out.println("this is the request about= "+request.about());
           NotificationModel notification = new NotificationModel();
           notification.setAbout(request.about());
           notification.setMentionName(request.mentionName());
           notification.setFilter(request.filter());
           notification.setSender(request.sender());
           notification.setColor(request.color());
           notification.setCategory(request.category());
           notification.setCreatedAt(LocalDate.now().toString());
           notification.setTitle(request.title());
           notification.setType(request.type());
           notification.setMessage(request.message());
           notification.setImageUrl(request.imageUrl());
           repository.save(notification);

            response.setMessage("notification Added Successfully");
        response.setErrorType(ErrorResponseType.Nothing);
        response.setReturnedBoolean(true);
        response.setObject(notification);
        response.setErrorCode("20000");
        response.setThereIsAnError(false);
        response.setReturnedInteger(null);
        response.setReturnedList(null);
        response.setReturnedString(null);
        response.setReturnedMultipartFile(null);
        return response;
    }




    public ResponseModel deleteNotification(@PathVariable("notificationId") String id) {
     int ID = Integer.parseInt(id);
            if (repository.existsById(ID)) {
                repository.deleteById(ID);
                response.setMessage("notification deleted Successfully");
                response.setErrorType(ErrorResponseType.Nothing);
                response.setReturnedBoolean(true);
                response.setObject(null);
                response.setErrorCode("20000");
                response.setThereIsAnError(false);
                response.setReturnedInteger(null);
                response.setReturnedList(null);
                response.setReturnedString(null);
                response.setReturnedMultipartFile(null);
            } else {
                response.setMessage("notification unfounded with this id");
                response.setErrorType(ErrorResponseType.Nothing);
                response.setReturnedBoolean(true);
                response.setObject(null);
                response.setErrorCode("40000");
                response.setThereIsAnError(true);
                response.setReturnedInteger(null);
                response.setReturnedList(null);
                response.setReturnedString(null);
                response.setReturnedMultipartFile(null);
            }
            return response;
        }



}

