package com.wigo.repositories;

import com.wigo.models.UserProfileModel;
import com.wigo.models.VotingModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VotingRepository extends JpaRepository<VotingModel,Integer> {
    Optional<VotingModel> findByVotedId(Integer id);
    Optional<VotingModel> findByUserId(Integer id);
}
