package com.wigo.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;



@NoArgsConstructor
@Getter
@Setter
public class AllMobileDownloadableDataModel {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "mobile_data__id_sequence",
            sequenceName = "mobile_data_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "mobile_data_id_sequence"
    )
    @Column(name = "id")
    private Integer id;
     List<CoachModel> coachs;
     List<DealsModel> deals;
     List<GymModel> gyms;
     List<NotificationModel> notifications;
     List<ReminderModel> reminders;
     List<ProductCategoryModel> productCategories;
     List<ProductModel> products;
     List<WorkoutTypeModel> workoutTypes;



}
