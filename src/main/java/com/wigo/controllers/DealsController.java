
package com.wigo.controllers;


import com.wigo.AppConstants;
import com.wigo.Helpers.RequestHolder;
import com.wigo.models.DealsModel;
import com.wigo.models.ErrorResponseType;
import com.wigo.models.ResponseModel;
import com.wigo.repositories.DealsRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController


public class DealsController {
    private final DealsRepository repository;


    public DealsController(DealsRepository repository) {
        this.repository = repository;

    }


    ResponseModel response = new ResponseModel();



    public ResponseModel getDealById( Integer id) {
        Optional<DealsModel> model=repository.findById(id);
        if(model.isPresent()){
            response.setMessage("Exist");

            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }else {
            response.setMessage("unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        return response;
    }
    @GetMapping("/getAllDeals")
    public List<DealsModel> getAllDeals() {
        return repository.findAll();
    }

    @PostMapping("/createDeal")
    public ResponseModel createDeal(@RequestBody RequestHolder.NewDealRequest request )  {

       Optional<DealsModel> dealsModel=repository.findByTitle(request.title());
        if(dealsModel.isEmpty()){
            DealsModel model = new DealsModel();
            model.setModifiedAt(request.modifiedAt());
           model.setCreatedAt(request.createdAt());
           model.setDate(request.date());
           model.setImage(request.image());
           model.setOwner(request.owner());
           model.setTitle(request.title());
           model.setGym(request.gym());
           model.setDescription(request.description());

            //ban
            //message
            repository.save(model);
            response.setMessage("deal Added Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }else {
            response.setMessage("deal already exist");
            response.setErrorType(ErrorResponseType.DataAlreadyExist);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Deal Created"));
        return response;
    }


    @PutMapping("/updateDeal")
    public ResponseModel updateDeal(@RequestBody RequestHolder.UpdateDealRequest request) {
        Optional<DealsModel> modelFinder = repository.findById(request.id());
        if (modelFinder.isPresent()){
            DealsModel model=modelFinder.get();
            model.setModifiedAt(request.modifiedAt());
            model.setDate(request.date());
            model.setImage(request.image());
            model.setOwner(request.owner());
            model.setTitle(request.title());
            model.setGym(request.gym());
            model.setDescription(request.description());
            //ban
            //message
            repository.save(model);
            response.setMessage("deal information updated Successfully");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(true);
            response.setObject(model);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }else {
            response.setMessage("model unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Deal Updated"));
        return response;
    }

    public ResponseModel deleteDeal(Integer id) {

        if (repository.existsById(id)) {
            repository.deleteById(id);
            response.setMessage("deal deleted Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setMessage("deal unfounded with this id");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Deal Deleted"));
        return response;
    }
}

