package com.wigo.repositories;

import com.wigo.models.AuthenticatedUser;
import com.wigo.models.GymModel;
import com.wigo.models.UserProfileModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthenticatedUserRepository extends JpaRepository<AuthenticatedUser,Integer> {
    Optional<AuthenticatedUser> findByUserId(Integer id);
    Optional<AuthenticatedUser> findByToken(String token);
    Optional<AuthenticatedUser> findByUserMail(String mail);
    Optional<AuthenticatedUser> findByLastSignInDate(String lastSignInDate);
}
