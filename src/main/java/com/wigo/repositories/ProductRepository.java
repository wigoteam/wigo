package com.wigo.repositories;


import com.wigo.models.ProductModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository
        extends JpaRepository<ProductModel,Integer> {
Optional <ProductModel> findByNameAndGym(String name,Integer gym);
List<ProductModel> findByGym(Integer gym);



}
