package com.wigo.repositories;


import com.wigo.models.NotificationModel;
import com.wigo.models.ReminderModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ReminderRepository
        extends JpaRepository<ReminderModel,Integer> {
    Optional<ReminderModel> findByTitle(String name);



}
