package com.wigo.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@Entity()
@RequiredArgsConstructor
@Getter
@Setter
public class AuthenticatedUser {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "authenticated_user_id_sequence",
            sequenceName = "authenticated_user_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "authenticated_user_id_sequence"
    )
    @Column(name = "id")
    private Integer id;
    private Integer userId;
    private String userMail;
    @Column(length = 2048)
    private String  token;
    private String  lastSignInDate;


}
