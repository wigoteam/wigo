package com.wigo.controllers;

import com.wigo.Helpers.RequestHolder;
import com.wigo.models.*;
import com.wigo.repositories.CoachRepository;
import com.wigo.repositories.SpecialUserRepository;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
@RestController

public class CoachController {
    private final CoachRepository coachRepository;
    private final SpecialUserRepository specialUserRepository;

    public CoachController(CoachRepository coachRepository, SpecialUserRepository specialUserRepository) {
        this.coachRepository = coachRepository;
        this.specialUserRepository = specialUserRepository;
    }
    public ResponseModel getGymById( Integer id) {
        Optional<CoachModel> model = coachRepository.findById(id);
        if (model.isPresent()) {
            response.setMessage("Exist");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model.get());
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
        } else {
            response.setMessage("unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
        }
        response.setReturnedInteger(null);
        response.setReturnedList(null);
        response.setReturnedString(null);
        response.setReturnedMultipartFile(null);
        return response;
    }





    public List<CoachModel> getAllCoachs() {
         return coachRepository.findAll();
    }


    ResponseModel response = new ResponseModel();





    public ResponseModel checkCoachExistByName(String email) {
        Optional<SpecialUserModel> model = specialUserRepository.findByMail(email);
        if (model.isPresent()) {
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setMessage("true");
            response.setObject(model.get());
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);

        } else {
            response.setErrorType(ErrorResponseType.Nothing);
            response.setMessage("false");
            response.setReturnedBoolean(false);
            response.setErrorCode("40000");
            response.setThereIsAnError(false);
            response.setObject(null);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);

        }
        return response;


    }

    public ResponseModel createCoach(RequestHolder.NewCoachRequest request) {

        if (!checkCoachExistByName(request.mail()).getReturnedBoolean()) {
            SpecialUserModel userModel =new SpecialUserModel();
            userModel.setMail(request.mail());
            userModel.setPassword(request.password());
            userModel.setCreatedAt(LocalDate.now().toString());
            userModel.setModifiedAt(LocalDate.now().toString());
            userModel.setRole(Role.COACH);
            specialUserRepository.save(userModel);
            Optional<SpecialUserModel> restoredModel=specialUserRepository.findByMail(request.mail());
                CoachModel model = new CoachModel();
                model.setBirthday(request.birthday());
                model.setCoachOfTheMonth(request.coachOfTheMonth());
                model.setDescription(request.description());
                model.setGender(request.gender());
                model.setGym(request.gym());
                model.setName(request.name());
                model.setWorkoutTypes(request.workoutTypes());
                model.setUpVotes(request.upVotes());
                model.setPhone(request.phone());
                model.setNationality(request.nationality());
                model.setLocation(request.location());
                model.setLevel(request.level());
                model.setImage(request.image());
                model.setDownVotes(request.downVotes());
                model.setCreatedAt(LocalDate.now().toString());
                model.setModifiedAt(LocalDate.now().toString());
                coachRepository.save(model);
                restoredModel.get().setOwnedId(model.getId().toString());
                specialUserRepository.save(restoredModel.get());
                response.setErrorType(ErrorResponseType.Nothing);
                response.setReturnedBoolean(true);
                response.setObject(model);
                response.setMessage("coach account created successfully");
                response.setErrorCode("20000");
                response.setThereIsAnError(false);
                response.setReturnedInteger(model.getId());
                response.setReturnedList(null);
                response.setReturnedString(null);
                response.setReturnedMultipartFile(null);
             } else {
            response.setErrorType(ErrorResponseType.CorruptedData);
            response.setMessage("coach mail already exist");
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("New Coach Created"));
        return response;

    }


    /******************************************************************************************************************/


    /****UPDATE NORMAL USER****/
    public ResponseModel updateCoach(RequestHolder.UpdateCoachRequest request) {

        Optional<CoachModel> modelFinder = coachRepository.findById(request.id());
        if (modelFinder.isPresent()) {
            CoachModel model = modelFinder.get();
            model.setBirthday(request.birthday());
            model.setCoachOfTheMonth(request.coachOfTheMonth());
            model.setDescription(request.description());
            model.setGender(request.gender());
            model.setGym(request.gym());
            model.setName(request.name());
            model.setWorkoutTypes(request.workoutTypes());
            model.setUpVotes(request.upVotes());
            model.setPhone(request.phone());
            model.setNationality(request.nationality());
            model.setLocation(request.location());
            model.setLevel(request.level());
            model.setImage(request.image());
            model.setDownVotes(request.downVotes());
            model.setModifiedAt(LocalDate.now().toString());

            //ban
            //message
            coachRepository.save(model);
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model);
            response.setMessage("coach account updated successfully");
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(model.getId());
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setMessage("coach unfounded");
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Coach updated"));
        return response;

    }

    public ResponseModel deleteCoach(Integer id) {

        if (coachRepository.existsById(id)) {
            coachRepository.deleteById(id);
            response.setMessage("coach account deleted Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setMessage("coach unfounded with this id");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Coach Deleted"));
        return response;
    }


}
