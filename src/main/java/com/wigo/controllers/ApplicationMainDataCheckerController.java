package com.wigo.controllers;

import com.wigo.Helpers.RequestHolder;
import com.wigo.models.*;
import com.wigo.repositories.ApplicationMainDataCheckerRepository;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.List;

@RestController

public class ApplicationMainDataCheckerController {
    private static ApplicationMainDataCheckerRepository repository;

    public ApplicationMainDataCheckerController(ApplicationMainDataCheckerRepository repository) {
        this.repository = repository;
    }


    public ResponseModel getLastDataBaseUpdateDate() {
        List<ApplicationMainDataChecker> modelList = repository.findAll();
        if (!modelList.isEmpty())
        {
            ApplicationMainDataChecker model=modelList.get(modelList.size()-1);
            response.setMessage(model.getDataBaseUpdatedAt().toString());
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
        }
        else {
            response.setMessage("unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
        }
        response.setReturnedInteger(null);
        response.setReturnedList(null);
        response.setReturnedString(null);
        response.setReturnedMultipartFile(null);
        return response;
    }






    ResponseModel response = new ResponseModel();


    public static void changeApplicationMainData(RequestHolder.ChangeApplicationMainData request) {
            ApplicationMainDataChecker model = new ApplicationMainDataChecker();
            String now = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date());
            model.setDataBaseUpdatedAt(now);
            model.setApplicationLog(request.applicationLog());
            repository.save(model);
    }
    public ResponseModel deleteApplicationMainData(String id) {

        if (repository.existsById(Integer.parseInt(id))) {
            repository.deleteById(Integer.parseInt(id));
            response.setMessage("application main data deleted Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setMessage("application main data unfounded with this id");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        return response;
    }


}
