package com.wigo.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Getter
@Setter
@Entity(name = "userProfile")

public class UserProfileModel  {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "user_profile_id_sequence",
            sequenceName = "user_profile_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "user_profile_id_sequence"
    )
    @Column(name = "id")
    private Integer id;

    private String firstName;
    private String lastName;
    private String mail;

    private String password;
    private String phone;
    private String image;
    private String nationality;
    private String birthday;
    private String gender;
    @Column(length = 2048)
    private String token;
    @Column(length = 2048)
    private String serialId;
    private String gym;//list
    private String height;
    private String weight;
    private String Gallery;//list
    private String chats;//list
    private String phoneType;
    private Boolean pro;
    @Enumerated(EnumType.STRING)
    private Role role;
    private String createdAt;
    private String modifiedAt;

    public UserProfileModel() {
    }

    public UserProfileModel(Integer id, String firstName, String lastName, String mail, String password, String phone, String image, String nationality, String birthday, String gender, String token, String serialId, String gym, String height, String weight, String gallery, String chats, String phoneType, Boolean pro, Role role, String createdAt, String modifiedAt) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.password = password;
        this.phone = phone;
        this.image = image;
        this.nationality = nationality;
        this.birthday = birthday;
        this.gender = gender;
        this.token = token;
        this.serialId = serialId;
        this.gym = gym;
        this.height = height;
        this.weight = weight;
        Gallery = gallery;
        this.chats = chats;
        this.phoneType = phoneType;
        this.pro = pro;
        this.role = role;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }



   }


