package com.wigo.models;

public enum  NotificationType{
    ALERT,
    ADMIN,
    ADS,
    INFORMATION
}