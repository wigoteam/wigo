package com.wigo.Helpers;

import com.wigo.AppConstants;
import com.wigo.controllers.*;
import com.wigo.models.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(AppConstants.API_BASE_URL)
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
public class ApiHandler {
    final WorkoutTypeController workoutTypeController;
    final UserProfileAuthenticationController userProfileAuthenticationController;
    final ProductController productController;
    final GymController gymController;
    final DealsController dealsController;
    final CoachController coachController;
    final CategoryController productCategoryController;
    final AllDownloadableDataController allDownloadableDataController;
    final SpecialUserController specialUserController;
    final NotificationController notificationController;
    final ReminderController reminderController;
    final VotingController votingController;
    final ApplicationMainDataCheckerController applicationMainDataCheckerController;
    ResponseModel response=new ResponseModel();

    //
    @PostMapping("/test")
    public ResponseModel testPostApi() {

        response.setMessage("api tested Successfully");
        response.setErrorCode("20000");
        response.setThereIsAnError(false);
        response.setErrorType(ErrorResponseType.Nothing);
        response.setReturnedInteger(1000);
        response.setReturnedBoolean(true);
        AdvantageModel advantageModel=new AdvantageModel();
        advantageModel.setDescription("fdfg");
        advantageModel.setTitle(" fdfdf");
        response.setObject(advantageModel);

        return response;
    }
    //


    @GetMapping("/workoutType/getWorkoutTypeById/{id}")
    public ResponseModel getWorkoutTypeById(@PathVariable String id) {
        return workoutTypeController.getWorkoutTypeById(id);
    }
    @GetMapping("/workoutType/getAllWorkoutTypes")
    public List<WorkoutTypeModel> getAllWorkoutTypes() {
        return workoutTypeController.getAllWorkoutTypes();
    }
    @PostMapping("/workoutType/createWorkoutType")
    public ResponseModel createWorkoutType(@RequestBody RequestHolder.NewWorkoutTypeRequest  request) {
        return workoutTypeController.createWorkoutType(request);
    }
    @PutMapping("/workoutType/updateWorkoutType")
    public ResponseModel updateWorkoutType(@RequestBody RequestHolder.UpdateWorkoutTypeRequest request) {
return workoutTypeController.updateWorkoutType(request);
    }
    @DeleteMapping("/workoutType/deleteWorkoutTypeById/{id}")
    public ResponseModel deleteWorkoutType(@PathVariable String id) {
        return workoutTypeController.deleteWorkoutType(id);
    }
    @GetMapping("/userProfile/getUserProfileById/{id}")
    public ResponseModel getUserProfileById(@PathVariable Integer id) {
        return userProfileAuthenticationController.getUserProfileById(id);
    }
    @PostMapping("/userProfile/Login")
    public ResponseModel loginNormalUser(@RequestBody RequestHolder.LoginRequest request) {
        return userProfileAuthenticationController.loginNormalUser(request);
    }
    @PostMapping("/userProfile/checkUserTokenValidity")
    public ResponseModel userIsConnected(@RequestBody String id){
        return userProfileAuthenticationController.userIsConnected(Integer.parseInt(id));
    }
    @PostMapping("/userProfile/PreLogin")
    public ResponseModel preLogin(@RequestBody RequestHolder.PreLoginRequest request) {
        return userProfileAuthenticationController.preLogin(request);
    }
    @GetMapping("/userProfile/getAllUsersProfile")
    public List<UserProfileModel> getAllUsersProfile() {
        return userProfileAuthenticationController.getAllUsersProfile();
    }
    @PostMapping("/userProfile/createUserProfile")
    public ResponseModel createUserProfile(@RequestBody RequestHolder.NewUserProfileRequest request) {
        return userProfileAuthenticationController.createUserProfile(request);
    }
    @PutMapping("/userProfile/updateUserProfile")
    public ResponseModel updateUserProfile(@RequestBody RequestHolder.UpdateUserProfile request) {
        return userProfileAuthenticationController.updateUserProfile(request);
    }
    @DeleteMapping("/userProfile/deleteUserProfile/{id}")
    public ResponseModel deleteUserProfile(@PathVariable String id) {
        return userProfileAuthenticationController.deleteUserProfile(Integer.parseInt(id));
    }
    @GetMapping("/product/getProductById/{id}")
    public ResponseModel getProductById(@PathVariable String id) {
        return productController.getProductById(Integer.parseInt(id));
    }
    @GetMapping("/product/getAllProducts")
    public List<ProductModel> getAllProducts() {
        return productController.getAllProducts();
    }
    @GetMapping("/product/getAllGymProducts/{id}")
    public List<ProductModel> getAllGymProducts(@PathVariable String id) {
        return productController.getGymProducts(Integer.parseInt(id));
    }
    @PostMapping("/product/createNewProduct")
    public ResponseModel createNewProduct(@RequestBody RequestHolder.NewProductRequest request) {
        return productController.createNewProduct(request);
    }
    @PutMapping("/product/updateProduct")
    public ResponseModel updateProduct(@RequestBody RequestHolder.UpdateProductRequest request) {
        return productController.updateProduct(request);
    }
    @DeleteMapping("/product/deleteProduct/{id}")
    public ResponseModel deleteProduct(@PathVariable("id") String id) {
        return productController.deleteProduct(Integer.parseInt(id));
    }




    @GetMapping("/gym/getGymById/{id}")
    public ResponseModel getGymById(@PathVariable String id) {
        return gymController.getGymById(Integer.parseInt(id));
    }


    @GetMapping("/gym/getAllGyms")
    public List<GymModel> getAllGyms() {
        return gymController.getAllGyms();
    }
    @PostMapping("/gym/createGym")
    public ResponseModel createGym(@RequestBody RequestHolder.NewGymRequest request) {
        return gymController.createGym(request);
    }
    @PutMapping("/gym/updateGym")
    public ResponseModel updateGym(@RequestBody RequestHolder.UpdateGymRequest request) {
        return gymController.updateGym(request);
    }
    @DeleteMapping("/gym/deleteGym/{id}")
    public ResponseModel deleteGym(@PathVariable("id") String id) {
        return gymController.deleteGym(Integer.parseInt(id));
    }

    @GetMapping("/deals/getDealById/{id}")
    public ResponseModel getDealById(@PathVariable String id) {
        return dealsController.getDealById(Integer.parseInt(id));
    }
    @GetMapping("/deals/getAllDeals")
    public List<DealsModel> getAllDeals() {
        return dealsController.getAllDeals();
    }
    @PostMapping("/deals/createDeal")
    public ResponseModel createDeal(@RequestBody RequestHolder.NewDealRequest request) {
        return dealsController.createDeal(request);
    }
    @PutMapping("/deals/updateDeal")
    public ResponseModel updateDeal(@RequestBody RequestHolder.UpdateDealRequest request) {
        return dealsController.updateDeal(request);
    }
    @DeleteMapping("/deals/deleteDeal/{id}")
    public ResponseModel deleteDeal(@PathVariable("id") String id) {
        return dealsController.deleteDeal(Integer.parseInt(id));
    }

    @GetMapping("/coach/getCoachById/{id}")
    public ResponseModel getCoachById(@PathVariable String id) {
        return coachController.getGymById(Integer.parseInt(id));
    }
    @GetMapping("/coach/getAllCoachs")
    public List<CoachModel> getAllCoachs() {
        return coachController.getAllCoachs();
    }
    @PostMapping("/coach/createCoach")
    public ResponseModel createCoach(@RequestBody RequestHolder.NewCoachRequest request) {
        return coachController.createCoach(request);
    }
    @PutMapping("/coach/updateCoach")
    public ResponseModel updateCoach(@RequestBody RequestHolder.UpdateCoachRequest request) {
        return coachController.updateCoach(request);
    }
    @DeleteMapping("/coach/deleteCoach/{id}")
    public ResponseModel deleteCoach(@PathVariable("id") String id) {
        return coachController.deleteCoach(Integer.parseInt(id));
    }
    @GetMapping("/productCategory/getProductCategoryById/{id}")
    public ResponseModel getProductCategoryById(@PathVariable String id) {
        return productCategoryController.getProductCategoryById(Integer.parseInt(id));
    }
    @GetMapping("/productCategory/getAllProductCategories")
    public List<ProductCategoryModel> getAllProductCategories() {
        return productCategoryController.getAllProductCategories();
    }
    @PostMapping("/productCategory/createProductCategory")
    public ResponseModel createProductCategory(@RequestBody RequestHolder.CreateProductCategoryRequest request) {
        return productCategoryController.createProductCategory(request);
    }
    @PutMapping("/productCategory/updateProductCategory")
    public ResponseModel updateProductCategory(@RequestBody RequestHolder.UpdateProductCategoryRequest request) {
        return productCategoryController.updateProductCategory(request);
    }
    @DeleteMapping("/productCategory/deleteProductCategory/{id}")
    public ResponseModel deleteProductCategory(@PathVariable("id") String id) {
        return productCategoryController.deleteProductCategory(Integer.parseInt(id));
    }
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/data/downloadMobileData")
    public ResponseModel createProductCategory(@RequestBody RequestHolder.DownloadMobileDataRequest request) {
        return allDownloadableDataController.getAllMobileDaTa(request);
    }

    @GetMapping("/SpecialUser/getSpecialUserById/{id}")
    public ResponseModel getSpecialUserById(@PathVariable Integer id) {
        return specialUserController.getSpecialUserById(id);
    }
    @PostMapping("/SpecialUser/Login")
    public ResponseModel loginSpecialUser(@RequestBody RequestHolder.LoginRequest request) {
        return specialUserController.loginSpecialUser(request);
    }

    @PostMapping("/SpecialUser/PreLogin")
    public ResponseModel specialUserPreLogin(@RequestBody RequestHolder.PreLoginRequest request) {
        return specialUserController.preLogin(request);
    }
    @GetMapping("/SpecialUser/getAllSpecialUsers")
    public List<SpecialUserModel> getAllSpecialUsers() {
        return specialUserController.getAllSpecialUsers();
    }
    @PostMapping("/SpecialUser/createSpecialUser")
    public ResponseModel createSpecialUser(@RequestBody RequestHolder.NewSpecialUserRequest request) {
        return specialUserController.createSpecialUser(request);
    }
    @PutMapping("/SpecialUser/updateSpecialUser")
    public ResponseModel updateSpecialUser(@RequestBody RequestHolder.UpdateSpecialUserRequest request) {
        return specialUserController.updateSpecialUser(request);
    }
    @DeleteMapping("/SpecialUser/deleteSpecialUser/{id}")
    public ResponseModel deleteSpecialUser(@PathVariable String id) {
        return specialUserController.deleteSpecialUser(Integer.parseInt(id));
    }

    @GetMapping("/notification/getAllNotification")
    public List<NotificationModel> getAllNotification() {
        return notificationController.getAllNotification();
    }
    @PostMapping("/notification/createNotification")
    public ResponseModel createNotification(@RequestBody RequestHolder.NewNotificationRequest  request) {
        return notificationController.createNotification(request);
    }
    @DeleteMapping("/notification/deleteNotification/{id}")
    public ResponseModel deleteNotification(@PathVariable String id) {
        return notificationController.deleteNotification(id);
    }
    @GetMapping("/reminder/getAllReminders")
    public List<ReminderModel> getAllReminders() {
        return reminderController.getAllReminders();
    }
    @PostMapping("/reminder/createReminder")
    public ResponseModel createReminder(@RequestBody RequestHolder.NewReminderRequest  request) {
        return reminderController.CreateReminder(request);
    }
    @DeleteMapping("/reminder/deleteReminder/{id}")
    public ResponseModel deleteReminder(@PathVariable String id) {
        return reminderController.removeReminder(id);
    }
    @PostMapping("/vote/createVote")
    public ResponseModel createVote(@RequestBody RequestHolder.VoteRequest  request) {
        return votingController.createVote(request);
    }
    @GetMapping("/vote/getAllVotes")
    public List<VotingModel> getAllVotes() {
        return votingController.getAllVotes();
    }
    @GetMapping("/mainData/getLastApplicationMainData")
    public ResponseModel getLastApplicationMainData() {
        return applicationMainDataCheckerController.getLastDataBaseUpdateDate();
    }
    @DeleteMapping("/mainData/deleteApplicationMainData/{id}")
    public ResponseModel deleteApplicationMainData(@PathVariable String id) {
        return applicationMainDataCheckerController.deleteApplicationMainData(id);
    }

    }
