package com.wigo.configs;

import com.wigo.AppConstants;
import com.wigo.models.UserProfileModel;
import com.wigo.repositories.UserProfileRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@Service
public class JwtService {


  private String secretKey= AppConstants.TOKEN_KEY;

  private long jwtExpiration=AppConstants.TOKEN_KEY_EXPIRATION;

  private long refreshExpiration=AppConstants.TOKEN_KEY_REFRESH_EXPIRATION;

  public String extractUsername(String token) {
    return extractClaim(token, Claims::getSubject);
  }

  public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
    final Claims claims = extractAllClaims(token);
    return claimsResolver.apply(claims);
  }

  public String generateToken(String email,
                              String name,
                              String phone,
                              String serialId) {
    return generateToken(
            new HashMap<>(),
            email,
            name,
            phone,
            serialId);
  }

  public String generateToken(
      Map<String, Object> extraClaims,
      String email,
      String name,
      String phone,
      String serialId
  ) {
    return buildToken(extraClaims,
            email,
            name,
            phone,
            serialId,
            jwtExpiration);
  }

  public String generateRefreshToken(
          String email,
          String name,
          String phone,
          String serialId
  ) {
    return buildToken(
            new HashMap<>(),
            email,
            name,
            phone,
            serialId,
            refreshExpiration);
  }

  private String buildToken(
          Map<String, Object> extraClaims,
          String email,
          String name,
          String phone,
          String serialId,
          long expiration
  ) {

    Map<String, Object> claims=new HashMap<>();
    claims.put("name",name);
    claims.put("phone",phone);
    claims.put("serialId",serialId);
    claims.put("extra",extraClaims);
    return Jwts
            .builder()
            .setClaims(claims)
            .setSubject(email)
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + expiration))
            .signWith(getSignInKey(), SignatureAlgorithm.HS256)
            .compact();
  }

  public boolean isTokenValid(String token,
                              UserProfileRepository userProfileRepository,
                              Optional<UserProfileModel> userDetails) {
    final String username = extractUsername(token);

    Optional<UserProfileModel> userProfileModel=userProfileRepository.findByMail(username);
    if (userProfileModel.isPresent()){
      return  !isTokenExpired(token);
    }else {
      return false;
    }
   }

  private boolean isTokenExpired(String token) {
    return extractExpiration(token).before(new Date());
  }

  private Date extractExpiration(String token) {
    return extractClaim(token, Claims::getExpiration);
  }

  private Claims extractAllClaims(String token) {
    return Jwts
        .parserBuilder()
        .setSigningKey(getSignInKey())
        .build()
        .parseClaimsJws(token)
        .getBody();
  }

  private Key getSignInKey() {
    byte[] keyBytes = Decoders.BASE64.decode(secretKey);
    return Keys.hmacShaKeyFor(keyBytes);
  }
}
