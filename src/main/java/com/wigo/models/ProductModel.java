package com.wigo.models;

import jakarta.persistence.*;


@Entity(name = "product")
public class ProductModel {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "product_id_sequence",
            sequenceName = "product_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "product_id_sequence"
    )

    @Column(name = "id")
    private Integer id;
    private String name;
    private String description;
    private String price;
    private String image;
    private String category;
    private Boolean isOnSale;
    private Double salePercent;
    private Integer gym;
    private Boolean isAvailable;
    private String createdAt;
    private String modifiedAt;

    public ProductModel(Integer id, String name, String description, String price, String image, String category, Boolean isOnSale, Double salePercent, Integer gym, Boolean isAvailable, String createdAt, String modifiedAt) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.image = image;
        this.category = category;
        this.isOnSale = isOnSale;
        this.salePercent = salePercent;
        this.gym = gym;
        this.isAvailable = isAvailable;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public ProductModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }



    public Boolean getOnSale() {
        return isOnSale;
    }

    public void setOnSale(Boolean onSale) {
        isOnSale = onSale;
    }

    public Double getSalePercent() {
        return salePercent;
    }

    public void setSalePercent(Double salePercent) {
        this.salePercent = salePercent;
    }

    public Integer getGym() {
        return gym;
    }

    public void setGym(Integer gym) {
        this.gym = gym;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }
}
