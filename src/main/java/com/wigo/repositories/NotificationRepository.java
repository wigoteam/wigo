package com.wigo.repositories;


import com.wigo.models.NotificationModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NotificationRepository
        extends JpaRepository<NotificationModel,Integer> {
    List<NotificationModel> findByTitle(String name);


}
