package com.wigo.repositories;


import com.wigo.models.WorkoutTypeModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WorkoutTypeRepository extends JpaRepository<WorkoutTypeModel,Integer> {

}
