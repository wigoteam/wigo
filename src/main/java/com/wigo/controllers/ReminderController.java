
package com.wigo.controllers;


import com.wigo.Helpers.RequestHolder;
import com.wigo.models.ErrorResponseType;
import com.wigo.models.ReminderModel;
import com.wigo.models.ResponseModel;
import com.wigo.repositories.ReminderRepository;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController

public class ReminderController {
    private final ReminderRepository repository;



    ResponseModel response = new ResponseModel();

    public ReminderController(ReminderRepository repository) {
        this.repository = repository;
    }












    public ResponseModel CreateReminder(RequestHolder.NewReminderRequest request) {
        ReminderModel reminderModel=new ReminderModel();
        reminderModel.setIsReminder(request.isReminder());
        reminderModel.setCreatedAt(LocalDate.now().toString());
        reminderModel.setReminderDate(request.reminderDate());
        reminderModel.setGyms(request.gyms());
        reminderModel.setTitle(request.title());
        reminderModel.setHeader(request.header());
        reminderModel.setModifiedAt(LocalDate.now().toString());
        reminderModel.setSpecial(request.special());
        reminderModel.setMessage(request.message());
        repository.save(reminderModel);

                    response.setMessage("reminder created");
                    response.setErrorType(ErrorResponseType.Nothing);
                    response.setReturnedBoolean(true);
                    response.setObject(reminderModel);
                    response.setErrorCode("20000");
                    response.setThereIsAnError(false);
                    response.setReturnedInteger(null);
                    response.setReturnedList(null);
                    response.setReturnedString(null);
                    response.setReturnedMultipartFile(null);


        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Reminder Created"));
        return response;
    }

    public List<ReminderModel> getAllReminders(){
        return repository.findAll();
    }


    public ResponseModel removeReminder(String id){
        Integer ID=Integer.parseInt(id);
        if (repository.existsById(ID)) {
            repository.deleteById(ID);
            response.setMessage("reminder deleted Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setMessage("reminder unfounded with this id");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);

        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Reminder Deleted"));
        return response;

    }











}

