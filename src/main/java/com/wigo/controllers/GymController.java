package com.wigo.controllers;

import com.wigo.Helpers.RequestHolder;
import com.wigo.models.*;
import com.wigo.repositories.GymRepository;
import com.wigo.repositories.SpecialUserRepository;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController

public class GymController {
    private final GymRepository gymRepository;

    private final SpecialUserRepository specialUserRepository;

    public GymController(GymRepository gymRepository, SpecialUserRepository specialUserRepository) {
        this.gymRepository = gymRepository;
        this.specialUserRepository = specialUserRepository;
    }






    public ResponseModel getGymById( Integer id) {
        Optional<GymModel> model = gymRepository.findById(id);
        if (model.isPresent()) {
            response.setMessage("Exist");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model.get());
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
        } else {
            response.setMessage("unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
        }
        response.setReturnedInteger(null);
        response.setReturnedList(null);
        response.setReturnedString(null);
        response.setReturnedMultipartFile(null);
        return response;
    }







    public List<GymModel> getAllGyms() {

        return gymRepository.findAll();
    }


    ResponseModel response = new ResponseModel();



    public ResponseModel checkGymExistByName(String email) {
        Optional<SpecialUserModel> model = specialUserRepository.findByMail(email);
        if (model.isPresent()) {
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setMessage("true");
            response.setObject(model.get());
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);

        } else {
            response.setErrorType(ErrorResponseType.Nothing);
            response.setMessage("false");
            response.setReturnedBoolean(false);
            response.setErrorCode("40000");
            response.setThereIsAnError(false);
            response.setObject(null);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);

        }
        return response;


    }

    public ResponseModel createGym(RequestHolder.NewGymRequest request) {

        if (!checkGymExistByName(request.mail()).getReturnedBoolean()) {
            SpecialUserModel model =new SpecialUserModel();
            model.setMail(request.mail());
            model.setPassword(request.password());
            model.setCreatedAt(LocalDate.now().toString());
            model.setModifiedAt(LocalDate.now().toString());
            model.setRole(Role.GYM);
            specialUserRepository.save(model);
            Optional<SpecialUserModel> restoredModel=specialUserRepository.findByMail(request.mail());
           if (restoredModel.isPresent()){
               GymModel gymModel = new GymModel();
               gymModel.setOwner(restoredModel.get().getId());
               gymModel.setAdvantages(request.advantages());
               gymModel.setCoachs(request.coachs());
               gymModel.setColor(request.color());
               gymModel.setImage(request.image());
               gymModel.setDescription(request.description());
               gymModel.setLatitude(request.latitude());
               gymModel.setLongitude(request.longitude());
               gymModel.setWorkoutTypes(request.workoutTypes());
               gymModel.setWorkingTime(request.workingTime());
               gymModel.setMondayPlan(request.mondayPlan());
               gymModel.setThursdayPlan(request.thursdayPlan());
               gymModel.setTuesdayPlan(request.tuesdayPlan());
               gymModel.setWednesdayPlan(request.wednesdayPlan());
               gymModel.setFridayPlan(request.fridayPlan());
               gymModel.setSaturdayPlan(request.saturdayPlan());
               gymModel.setSundayPlan(request.sundayPlan());
               gymModel.setPhone(request.phone());
               gymModel.setName(request.name());
               gymModel.setProducts(request.products());
               gymModel.setMoreImages(request.moreImages());
               gymModel.setMemberships(request.memberships());
               gymModel.setCreatedAt(LocalDate.now().toString());
               gymModel.setModifiedAt(LocalDate.now().toString());
               gymRepository.save(gymModel);
               restoredModel.get().setOwnedId(model.getId().toString());
               specialUserRepository.save(restoredModel.get());
               response.setErrorType(ErrorResponseType.Nothing);
               response.setReturnedBoolean(true);
               response.setObject(gymModel);
               response.setMessage("gym created successfully");
               response.setErrorCode("20000");
               response.setThereIsAnError(false);
               response.setReturnedInteger(gymModel.getId());
               response.setReturnedList(null);
               response.setReturnedString(null);
               response.setReturnedMultipartFile(null);
           }
        } else {
            response.setErrorType(ErrorResponseType.CorruptedData);
            response.setMessage("gym name already exist");
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }

        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("new Gym Created"));
        return response;

    }


    /******************************************************************************************************************/



    /****UPDATE NORMAL USER****/
    public ResponseModel updateGym(RequestHolder.UpdateGymRequest request) {

        Optional<GymModel> gymModelFinder = gymRepository.findById(request.id());
        if (gymModelFinder.isPresent()) {
            GymModel gymModel = gymModelFinder.get();
            gymModel.setAdvantages(request.advantages());
            gymModel.setCoachs(request.coachs());
            gymModel.setColor(request.color());
            gymModel.setImage(request.image());
            gymModel.setDescription(request.description());
            gymModel.setLatitude(request.latitude());
            gymModel.setLongitude(request.longitude());
            gymModel.setWorkoutTypes(request.workoutTypes());
            gymModel.setWorkingTime(request.workingTime());
            gymModel.setMondayPlan(request.mondayPlan());
            gymModel.setThursdayPlan(request.thursdayPlan());
            gymModel.setTuesdayPlan(request.tuesdayPlan());
            gymModel.setWednesdayPlan(request.wednesdayPlan());
            gymModel.setFridayPlan(request.fridayPlan());
            gymModel.setSaturdayPlan(request.saturdayPlan());
            gymModel.setSundayPlan(request.sundayPlan());
            gymModel.setPhone(request.phone());
            gymModel.setName(request.name());
            gymModel.setMoreImages(request.moreImages());
            gymModel.setProducts(request.products());
            gymModel.setMemberships(request.memberships());
            gymModel.setModifiedAt(LocalDate.now().toString());
            //ban
            //message
            gymRepository.save(gymModel);
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(gymModel);
            response.setMessage("gym updated successfully");
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(gymModel.getId());
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setMessage("gym unfounded");
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Gym updated"));
        return response;
    }

    public ResponseModel deleteGym(Integer id) {

        if (gymRepository.existsById(id)) {
            gymRepository.deleteById(id);
            response.setMessage("gym deleted Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setMessage("gym unfounded with this id");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Gym deleted"));
        return response;
    }


}
