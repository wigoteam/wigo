package com.wigo.models;

public enum ErrorResponseType {
    Nothing,
    NoDataFound,
    CorruptedData,
    WrongPassword,
    DataAlreadyExist

}
