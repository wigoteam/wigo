package com.wigo.models;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WorkingTimeModel {

    private String MondayTime;

    private String TuesdayTime;

    private String WednesdayTime;

    private String ThursdayTime;

    private String FridayTime;
    private String SaturdayTime;
    private String SundayTime;

    @Override
    public String toString() {
        ObjectMapper objectMapper=new ObjectMapper();
        return "{"+
                "\"MondayTime\":\"" + MondayTime + "\"" +
                ",\" TuesdayTime\":\"" + TuesdayTime + "\"" +
                ",\" WednesdayTime\":\"" + WednesdayTime +"\"" +
                ",\" ThursdayTime=\":\"" + ThursdayTime +"\"" +
                ",\" FridayTime\":\"" + FridayTime + "\"" +
                ",\" SaturdayTime\":\"" + SaturdayTime +"\"" +
                ",\" SundayTime\":\"" + SundayTime + "\"" +
                "}";
    }
}
