package com.wigo.repositories;

import com.wigo.models.GymModel;
import com.wigo.models.UserProfileModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GymRepository extends JpaRepository<GymModel,Integer> {
    Optional<GymModel> findByName(String name);
}
