package com.wigo;

import com.wigo.Helpers.RequestHolder;
import com.wigo.configs.JwtService;
import com.wigo.controllers.*;
import com.wigo.models.*;
import io.jsonwebtoken.Claims;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.*;

import static com.wigo.Utils.returnListFromString;

@SpringBootApplication
@RestController
@RequestMapping("/api/v1")
public class Main {

    private EmailSenderController senderController;
    private ApplicationMainDataCheckerController applicationMainDataCheckerController;
    private NotificationController notificationController;
    private GymController gymController;
    private CoachController coachController;
    private SpecialUserController specialUserController;
    private UserProfileAuthenticationController userProfileAuthenticationController;
    private ReminderController reminderController;
    private CategoryController categoryController;
    private WorkoutTypeController workoutTypeController;
    private ProductController productController;
    private DealsController dealsController;
    private JwtService jwtService;

    public Main(EmailSenderController senderController, ApplicationMainDataCheckerController applicationMainDataCheckerController, NotificationController notificationController, GymController gymController, CoachController coachController, SpecialUserController specialUserController, UserProfileAuthenticationController userProfileAuthenticationController, ReminderController reminderController, CategoryController categoryController, WorkoutTypeController workoutTypeController, ProductController productController, DealsController dealsController, JwtService jwtService) {
        this.senderController = senderController;
        this.applicationMainDataCheckerController = applicationMainDataCheckerController;
        this.notificationController = notificationController;
        this.gymController = gymController;
        this.coachController = coachController;
        this.specialUserController = specialUserController;
        this.userProfileAuthenticationController = userProfileAuthenticationController;
        this.reminderController = reminderController;
        this.categoryController = categoryController;
        this.workoutTypeController = workoutTypeController;
        this.productController = productController;
        this.dealsController = dealsController;
        this.jwtService = jwtService;
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }


    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        initFakeData();
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 250;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        System.out.println(generatedString);
        System.out.println(returnListFromString("------ali--- ---"));
        System.out.println("Hello Mohamed ,I have just started up, i will initial your server fake data right now, good luck , i will inform you when i finish");
        System.out.println("This is fast right ? :p");
        UserProfileModel userProfileModel = new UserProfileModel(

                1,
                "firstName", "lastName", "mail", "password", "phone", "image", "Tunisian", "birthday", "male", "token", "serialId", "", "180", "80", "f","chats", "phoneType", true, Role.USER, "createdAt", "modifiedAt");

String token=jwtService
        .generateToken(
                "email",
                "name",
                "phone",
                "serialId"
        );
        System.out.println("This is "+token
                );
        System.out.println(

                Optional.ofNullable((jwtService.extractClaim(token, Claims::getSubject)))
        );


        ResponseModel responseModel=new ResponseModel();
        System.out.println(notificationController.getAllNotification());



}

public void initFakeData(){
        notificationController.createNotification(
                new RequestHolder.NewNotificationRequest(

                                NotificationType.INFORMATION.name(),
                                "Update 3.1.0",
                                "Application Update",
                                "category",
                                "@Hama",
                                "Admin",
                                "When you have time please take a look at the update news section to know the last update details. ",
                                "assets/avatars/avatar-1.png",
                                "BBF1C3",
                                "",
                                LocalDate.now().toString()
                )
        );
        //👋
    notificationController.createNotification(
            new RequestHolder.NewNotificationRequest(

                    NotificationType.INFORMATION.name(),
                    "Coach Sami is absent",
                    "Coach absence",
                    "category",
                    "",
                    "Admin",
                    "The coach Sami will be absent today, you can come to the gym and join the coach Alia, thank you!!!",
                    "assets/avatars/avatar-2.png",
                    "FAA3FF",
                    "1",
                    LocalDate.now().toString()
            )
    );
    notificationController.createNotification(
            new RequestHolder.NewNotificationRequest(

                    NotificationType.INFORMATION.name(),
                    "Water bottle founded",
                    "Item founded",
                    "category",
                    "",
                    "Admin",
                    "if any one forget his bottle today in the gym,please contact the lost & found reception to restore it -in the second floor-",
                    "assets/avatars/avatar-3.png",
                    "FAA3FF",
                    "2",
                    LocalDate.now().toString()
            )
    );
    notificationController.createNotification(
            new RequestHolder.NewNotificationRequest(

                    NotificationType.ADS.name(),
                    "Free water bottle in gym 1",
                    "Announce",
                    "category",
                    "",
                    "Admin",
                    "A free premium water bottle like a gift for the best visitor today",
                    "assets/avatars/avatar-4.png",
                    "FAA3FF",
                    "",
                    LocalDate.now().toString()
            )
    );
    coachController.createCoach(
            new RequestHolder.NewCoachRequest(
                    "Ali",
                    "ali@gmail.com",
                    "95503411",
                    "95503411",
                    "imageFakeUrl",
                    LocalDate.now().minusYears(20).toString(),
                    "male",
                    1,
                    true,
                    50,
                    0,
                    "Tunis",
                    "A coach to give you better health",
                    "Tunisian",
                    "Level 3",
                    LocalDate.now().toString(),
                    LocalDate.now().toString(),
                    "1---2"
            )
    );
    coachController.createCoach(
            new RequestHolder.NewCoachRequest(
                    "Zahredine",
                    "zahredine@gmail.com",
                    "97911223",
                    "97911223",
                    "imageFakeUrl",
                    LocalDate.now().minusYears(20).toString(),
                    "male",
                    1,
                    false,
                    250,
                    700,
                    "Tunis",
                    "A coach to give you better experience",
                    "Tunisian",
                    "Level 3",
                    LocalDate.now().toString(),
                    LocalDate.now().toString(),
                    "0---2---1"
            )
    );
    coachController.createCoach(
            new RequestHolder.NewCoachRequest(
                    "Raid",
                    "raid@gmail.com",
                    "20202020",
                    "20202020",
                    "imageFakeUrl",
                    LocalDate.now().minusYears(20).toString(),
                    "male",
                    2,
                    true,
                    1050,
                    10,
                    "Tunis",
                    "A coach",
                    "Tunisian",
                    "Level 10",
                    LocalDate.now().toString(),
                    LocalDate.now().toString(),
                    "1---2"
            )
    );
    WorkingTimeModel workingTimeModel=new WorkingTimeModel();
    workingTimeModel.setMondayTime("8am-00:00am");
    workingTimeModel.setTuesdayTime("8am-00:00am");
    workingTimeModel.setWednesdayTime("8am-00:00am");
    workingTimeModel.setThursdayTime("8am-00:00am");
    workingTimeModel.setFridayTime("8am-00:00am");
    workingTimeModel.setSaturdayTime("8am-00:00am");
    workingTimeModel.setSundayTime("Closed");

    GymDailyEventModel gymDailyEventModel=new GymDailyEventModel();
    gymDailyEventModel.setName("feet work");
    gymDailyEventModel.setDescription("a good workout");
    gymDailyEventModel.setCoach(1);
    gymDailyEventModel.setImage("fake image");
    gymDailyEventModel.setType(1);
    gymDailyEventModel.setEventIsSpecial(false);
    gymDailyEventModel.setStartHour("8:00");
    gymDailyEventModel.setEndHour("10:00");

    GymDailyEventModel gymDailyEventModel2=new GymDailyEventModel();
    gymDailyEventModel2.setName("hard work");
    gymDailyEventModel2.setDescription("best workout");
    gymDailyEventModel2.setCoach(1);
    gymDailyEventModel2.setImage("fake image");
    gymDailyEventModel2.setType(1);
    gymDailyEventModel2.setEventIsSpecial(false);
    gymDailyEventModel2.setStartHour("10:00");
    gymDailyEventModel2.setEndHour("12:00");

    GymDailyEventModel gymDailyEventModel3=new GymDailyEventModel();
    gymDailyEventModel3.setName("back work");
    gymDailyEventModel3.setDescription("a workout");
    gymDailyEventModel3.setCoach(1);
    gymDailyEventModel3.setImage("fake image");
    gymDailyEventModel3.setType(1);
    gymDailyEventModel3.setEventIsSpecial(false);
    gymDailyEventModel3.setStartHour("16:00");
    gymDailyEventModel3.setEndHour("18:00");

    GymDailyEventModel gymDailyEventModel4=new GymDailyEventModel();
    gymDailyEventModel4.setName("unl,pw work");
    gymDailyEventModel4.setDescription("unknown workout");
    gymDailyEventModel4.setCoach(1);
    gymDailyEventModel4.setImage("fake image");
    gymDailyEventModel4.setType(1);
    gymDailyEventModel4.setEventIsSpecial(false);
    gymDailyEventModel4.setStartHour("18:00");
    gymDailyEventModel4.setEndHour("20:00");

    GymPlaningModel gymPlaningModel =new GymPlaningModel();
    gymPlaningModel.setPlaningDay(LocalDate.now().toString());
    gymPlaningModel.setListOfGymEvents(List.of(gymDailyEventModel,gymDailyEventModel2,gymDailyEventModel3,gymDailyEventModel4));
    GymPlaningModel gymPlaningModel1 =new GymPlaningModel();
    gymPlaningModel1.setPlaningDay(LocalDate.now().plusDays(1).toString());
    gymPlaningModel1.setListOfGymEvents(List.of(gymDailyEventModel,gymDailyEventModel2));
    AdvantageModel advantageModel1=new AdvantageModel();
    advantageModel1.setTitle("advantage 1 title");
    advantageModel1.setDescription("advantage 1 description");
    AdvantageModel advantageModel2=new AdvantageModel();
    advantageModel2.setTitle("advantage 2 title");
    advantageModel2.setDescription("advantage 2 description");
    AdvantageModel advantageModel3=new AdvantageModel();
    advantageModel3.setTitle("advantage 3 title");
    advantageModel3.setDescription("advantage 3 description");

    gymController.createGym(
            new RequestHolder.NewGymRequest(
                    "California Gym",
                    80.10,
                    10.22,
                     workingTimeModel.toString(),
                      "the best gym ever",
                      "50505050",
                      "california@gmail.com",
                      "50505050",
                    gymDailyEventModel2.toString()+"---"+gymDailyEventModel2.toString(),
                    gymDailyEventModel.toString()+"---"+gymDailyEventModel2.toString(),
                    gymDailyEventModel3.toString()+"---"+gymDailyEventModel2.toString(),
                    gymDailyEventModel3.toString()+"---"+gymDailyEventModel3.toString(),
                    gymDailyEventModel2.toString(),
                    gymDailyEventModel4.toString()+"---"+gymDailyEventModel2.toString(),
                    gymDailyEventModel4.toString()+"---"+gymDailyEventModel3.toString(),
                       "1---2",
                        advantageModel1.toString()+"---"+advantageModel2.toString(),
                        "1---2",
                        "FAA3FF",
                        "fake",
                        "1",
                        "fake1---fake2",
                        "",
                        LocalDate.now().toString(),
                        LocalDate.now().toString()

            )
    );
    gymController.createGym(
            new RequestHolder.NewGymRequest(
                    "Tunis Gym",
                    30.10,
                    50.32,
                    workingTimeModel.toString(),
                    "the tunisian gym ever",
                    "20202020",
                    "tunisgym@gmail.com",
                    "20202020",
                    gymDailyEventModel2.toString()+"---"+gymDailyEventModel2.toString(),
                    gymDailyEventModel.toString()+"---"+gymDailyEventModel2.toString(),
                    gymDailyEventModel3.toString()+"---"+gymDailyEventModel2.toString(),
                    gymDailyEventModel3.toString()+"---"+gymDailyEventModel3.toString(),
                    gymDailyEventModel2.toString(),
                    gymDailyEventModel4.toString()+"---"+gymDailyEventModel2.toString(),
                    gymDailyEventModel4.toString()+"---"+gymDailyEventModel3.toString(),
                    "0",
                    "",
                    "1",
                    "FAA3FF",
                    "fake",
                    "0---1---2",
                    "fake1---fake2",
                    "",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()

            )
    );

    workoutTypeController.createWorkoutType(
            new RequestHolder.NewWorkoutTypeRequest(
                    "workout type 1",
                    "FAA3FF",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )

    );
    workoutTypeController.createWorkoutType(
            new RequestHolder.NewWorkoutTypeRequest(
                    "workout type 2",
                    "BBF1C3",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )

    );
    workoutTypeController.createWorkoutType(
            new RequestHolder.NewWorkoutTypeRequest(
                    "workout type 3",
                    "BBF1C3",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )

    );
    workoutTypeController.createWorkoutType(
            new RequestHolder.NewWorkoutTypeRequest(
                    "workout type 4",
                    "FFC5D5",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )

    );
    reminderController.CreateReminder(
            new RequestHolder.NewReminderRequest(
                    "Reminder",
                    "Gym opening",
                    "Gym X will be in Sfax soon",
                    "",
                    true,
                    true, LocalDate.of(2023,12,1).toString(),
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )
    );
    reminderController.CreateReminder(
            new RequestHolder.NewReminderRequest(
                    "Reminder",
                    "Gym opening",
                    "Gym X will be in Tataouine soon",
                    "",
                    true,
                    true, LocalDate.of(2024,5,1).toString(),
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )
    );
    reminderController.CreateReminder(
            new RequestHolder.NewReminderRequest(
                    "Reminder",
                    "Gym Upgrading",
                    "Gym Z added 9 machines ,test it now",
                    "3",
                    false,
                    false, LocalDate.of(2023,12,1).toString(),
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )
    );
    reminderController.CreateReminder(
            new RequestHolder.NewReminderRequest(
                    "Announce",
                    "Gym Closing",
                    "Gym D will close until 21 december for maintenance reason ",
                    "3",
                    false,
                    false, LocalDate.of(2023,12,21).toString(),
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )
    );
    categoryController.createProductCategory(
            new RequestHolder.CreateProductCategoryRequest(
                    "Tshirts",
                    "image",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()

            )
    );
    categoryController.createProductCategory(
            new RequestHolder.CreateProductCategoryRequest(
                    "Jeans",
                    "image",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()

            )
    );
    categoryController.createProductCategory(
            new RequestHolder.CreateProductCategoryRequest(
                    "Shoes",
                    "image",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()

            )
    );
    categoryController.createProductCategory(
            new RequestHolder.CreateProductCategoryRequest(
                    "Jackets",
                    "image",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()

            )
    );
    categoryController.createProductCategory(
            new RequestHolder.CreateProductCategoryRequest(
                    "Socks",
                    "image",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()

            )
    );
    userProfileAuthenticationController.createUserProfile(
            new RequestHolder.NewUserProfileRequest(
                     "mohamed",
                     "amine",
                     "mohamed.amine.devo@gmail.com",
                     "94970082rR",
                     "94970082",
                     "fakeImage",
                     "Tunisian",
                    LocalDate.of(2000,12,18).toString(),
                    Genders.Male.name(),
                    "",
                    "",
                    "1",
                    "182",
                    "80",
                    "fake1---fake2",
                    "",
                    "Android",
                    false,
                    Role.USER,
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )
    );

    dealsController.createDeal(
            new RequestHolder.NewDealRequest(
                    "The best price",
                    "Gym x give you the best price ever,use Xmax code to earn a -20% coupon in all products",
                    "fake",
                    "1",
                    LocalDate.of(2023,9,25).toString(),
                    "Admin",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )
    );
    dealsController.createDeal(
            new RequestHolder.NewDealRequest(
                    "Black friday reduction",
                    "use BlackWigo code to earn a -40% coupon in all products",
                    "fake",
                    "",
                    LocalDate.of(2023,9,25).toString(),
                    "Admin",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )
    );
    dealsController.createDeal(
            new RequestHolder.NewDealRequest(
                    "The last deal",
                    "buy the last product in shop and earn a free pass in gym x and y ",
                    "fake",
                    "0---1",
                    "",
                    "Admin",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )
    );
    productController.createNewProduct(
            new RequestHolder.NewProductRequest(
                    "product x",
                    "Zara",
                    "50.3",
                    "dddd",
                    "1",
                    true,
                    10.0,
                    1,
                    true,
                    LocalDate.now().toString(),
                    LocalDate.now().toString()

            )
    );
    productController.createNewProduct(
            new RequestHolder.NewProductRequest(
                    "product Y",
                    "Pull&Bear",
                    "10.3",
                    "dddd",
                    "1",
                    true,
                    10.0,
                    1,
                    true,
                    LocalDate.now().toString(),
                    LocalDate.now().toString()

            )
    );
    productController.createNewProduct(
            new RequestHolder.NewProductRequest(
                    "Water bottle",
                    "Gold",
                    "20.3",
                    "dddd",
                    "2",
                    false,
                    10.0,
                    1,
                    true,
                    LocalDate.now().toString(),
                    LocalDate.now().toString()

            )
    );
    productController.createNewProduct(
            new RequestHolder.NewProductRequest(
                    "product koko",
                    "Zara",
                    "250.3",
                    "dddd",
                    "2",
                    false,
                    10.0,
                    1,
                    true,
                    LocalDate.now().toString(),
                    LocalDate.now().toString()

            )
    );
    productController.createNewProduct(
            new RequestHolder.NewProductRequest(
                    "product vvrvfvf",
                    "Zara",
                    "570.3",
                    "dddd",
                    "1",
                    true,
                    10.0,
                    2,
                    true,
                    LocalDate.now().toString(),
                    LocalDate.now().toString()

            )
    );
    productController.createNewProduct(
            new RequestHolder.NewProductRequest(
                    "product gtbgdfgf",
                    "Zara",
                    "5210.3",
                    "dddd",
                    "3",
                    true,
                    10.0,
                    2,
                    true,
                    LocalDate.now().toString(),
                    LocalDate.now().toString()

            )
    );
    applicationMainDataCheckerController.changeApplicationMainData(
           new RequestHolder.ChangeApplicationMainData (


                   "server started"


           )
    );
    specialUserController.createSpecialUser(
            new RequestHolder.NewSpecialUserRequest(
                    "admin@gmail.com",
                    "94970082",
                    Role.ADMIN,
                    "0",
                    LocalDate.now().toString(),
                    LocalDate.now().toString()
            )
    );
    specialUserController.createSpecialUser(
            new RequestHolder.NewSpecialUserRequest(
                    "gym1@gmail.com",
                   "94970082",
                    Role.GYM,
                    "1",
                     LocalDate.now().toString(),
                     LocalDate.now().toString()
            )
    );
}


/*    @EventListener(ApplicationReadyEvent.class)
    public void senMail() throws MessagingException {
        System.out.println("i will try to send this mail");
        senderController.sendEmail();
    }*/




}
