package com.wigo.repositories;

import com.wigo.models.UserProfileModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserProfileRepository extends JpaRepository<UserProfileModel,Integer> {
    Optional<UserProfileModel> findByMail(String email);
    Optional<UserProfileModel> findByPhone(String phone);
}
