package com.wigo.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@RequiredArgsConstructor
@Getter
@Setter
@Entity(name = "deals")
public class DealsModel {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "deal_id_sequence",
            sequenceName = "deal_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "deal_id_sequence"
    )

    @Column(name = "id")
    private Integer id;
    private   String title;
    private    String description;
    private   String image;
    private  String gym;
    private   String date;
    private   String owner;
    private String createdAt;
    private String modifiedAt;


}
