package com.wigo.configs;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
public class CorsConfig implements CorsConfigurationSource {

    @Override
    public CorsConfiguration getCorsConfiguration(HttpServletRequest request) {
        System.out.println(request);
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedOrigins(Arrays.asList("**"));
        config.setAllowedMethods(Arrays.asList("**"));
        config.setAllowedHeaders(Arrays.asList("**"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);

        return config;
    }
}