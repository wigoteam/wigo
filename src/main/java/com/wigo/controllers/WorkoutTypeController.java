package com.wigo.controllers;


import com.wigo.Helpers.RequestHolder;
import com.wigo.models.*;
import com.wigo.repositories.WorkoutTypeRepository;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController

public class WorkoutTypeController {
    private final WorkoutTypeRepository workoutTypeRepository;

    public WorkoutTypeController(WorkoutTypeRepository workoutTypeRepository) {
        this.workoutTypeRepository = workoutTypeRepository;
    }






    public ResponseModel getWorkoutTypeById(String id) {

        Optional<WorkoutTypeModel> model = workoutTypeRepository.findById(Integer.parseInt(id));
        if (model.isPresent()) {
            response.setMessage("Exist");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model.get());
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
        } else {
            response.setMessage("unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
        }
        response.setReturnedInteger(null);
        response.setReturnedList(null);
        response.setReturnedString(null);
        response.setReturnedMultipartFile(null);
        return response;
    }


    public List<WorkoutTypeModel> getAllWorkoutTypes() {
        return workoutTypeRepository.findAll();
    }

    ResponseModel response = new ResponseModel();







    public ResponseModel createWorkoutType(RequestHolder.NewWorkoutTypeRequest request) {


            WorkoutTypeModel model = new WorkoutTypeModel();
            model.setWorkoutTypeColor(request.workoutTypeColor());
            model.setWorkoutTypeName(request.workoutTypeName());
            model.setCreatedAt(LocalDate.now().toString());
            model.setModifiedAt(LocalDate.now().toString());
            //ban
            //message
            workoutTypeRepository.save(model);
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model);
            response.setMessage("workout type created successfully");
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(model.getId());
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Workout type Created"));
        return response;
    }



    public ResponseModel updateWorkoutType( RequestHolder.UpdateWorkoutTypeRequest request) {

        Optional<WorkoutTypeModel> modelFinder = workoutTypeRepository.findById(request.id());
        if (modelFinder.isPresent()) {
            WorkoutTypeModel model = modelFinder.get();
            model.setWorkoutTypeName(request.workoutTypeName());
            model.setWorkoutTypeColor(request.workoutTypeColor());
            model.setModifiedAt(LocalDate.now().toString());
            workoutTypeRepository.save(model);
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model);
            response.setMessage("workout type updated successfully");
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(model.getId());
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setMessage("workout type unfounded");
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Workout Type updated"));
        return response;
    }


    public ResponseModel deleteWorkoutType(String id) {
        int ID = Integer.parseInt(id);
        if (workoutTypeRepository.existsById(ID)) {
            workoutTypeRepository.deleteById(ID);
            response.setMessage("workout type deleted Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setMessage("workout type unfounded with this id");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Workout Type Deleted"));
        return response;
    }
}
