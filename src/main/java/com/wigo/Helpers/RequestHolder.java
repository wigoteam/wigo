package com.wigo.Helpers;

import com.wigo.models.Role;
import jakarta.persistence.Column;

import java.time.LocalDate;
import java.util.Date;

public class RequestHolder {
    record TestApiRequest(Integer id, String string) { }
    public record PreLoginRequest(String email) { }
    public record LoginRequest(String email, String password){ }
    public record NewWorkoutTypeRequest(String workoutTypeName, String workoutTypeColor, String createdAt, String modifiedAt) { }
    public record UpdateWorkoutTypeRequest(Integer id, String workoutTypeName, String workoutTypeColor, String createdAt, String modifiedAt) { }
    public record NewUserProfileRequest(String firstName, String lastName, String mail, String password, String phone, String image, String nationality, String birthday, String gender, String token, String serialId, String gym, String height, String weight, String gallery, String chats, String phoneType, Boolean pro, Role role, String createdAt, String modifiedAt) { }
    public record UpdateUserProfile(Integer id, String firstName, String lastName, String mail, String password, String phone, String image, String nationality, String birthday, String gender, String token, String serialId, String gym, String height, String weight, String gallery, String chats, String phoneType, Boolean pro, Role role,  String modifiedAt) { }
    public record NewProductRequest(String name, String description, String price, String image, String category, Boolean isOnSale, Double salePercent, Integer gym, Boolean isAvailable, String createdAt, String modifiedAt) {}
    public record UpdateProductRequest(Integer id,String name, String description, String price, String image, String category, Boolean isOnSale, Double salePercent, Integer gym, Boolean isAvailable, String modifiedAt) { }
    public record NewPlaningRequest(Integer gymName, String planingDay, String listOfGymEvents, String createdAt, String modifiedAt) { }
    public record UpdatePlaningRequest(Integer id,Integer gymName, String planingDay, String listOfGymEvents, String modifiedAt) { }
    public record NewNotificationRequest( String type, String title, String about, String category, String mentionName, String sender, String message, String imageUrl, String color, String filter, String createdAt) {  }
 public record NewGymRequest( String name, Double longitude, Double latitude, String workingTime, String description, String phone, String mail, String password,    String mondayPlan,String tuesdayPlan, String wednesdayPlan, String thursdayPlan, String fridayPlan, String saturdayPlan, String sundayPlan,String products, String advantages, String coachs, String color, String image, String workoutTypes, String moreImages, String memberships, String createdAt, String modifiedAt) { }
    public record UpdateGymRequest(Integer id,String name,Double longitude, Double latitude, String workingTime, String description, String phone, String mail, String password,String mondayPlan,String tuesdayPlan, String wednesdayPlan, String thursdayPlan, String fridayPlan, String saturdayPlan, String sundayPlan, String products, String advantages, String coachs, String color, String image, String workoutTypes, String moreImages, String memberships,String modifiedAt) { }
    public record NewDealRequest(String title, String description, String image,String gym, String date, String owner, String createdAt, String modifiedAt) { }
    public record UpdateDealRequest(Integer id, String title, String description, String image,String gym, String date, String owner, String modifiedAt) { }
    public record NewCoachRequest(String name, String mail, String password, String phone, String image, String birthday, String gender, Integer gym, Boolean coachOfTheMonth, Integer upVotes, Integer downVotes, String location, String description, String nationality, String level, String createdAt, String modifiedAt, String workoutTypes) { }
    public record UpdateCoachRequest(Integer id, String name, String mail, String password, String phone, String image, String birthday, String gender, Integer gym, Boolean coachOfTheMonth, Integer upVotes, Integer downVotes, String location, String description, String nationality, String level, String modifiedAt, String workoutTypes) { }
    public record CreateProductCategoryRequest( String categoryName, String image, String createdAt, String modifiedAt){}
    public record UpdateProductCategoryRequest(Integer id, String categoryName, String image, String modifiedAt){}
    public record NewConnexionRequest(Integer userId, String token, String mail){}
    public record NewSpecialUserRequest(String mail, String password, Role role, String ownedId, String createdAt, String modifiedAt){}
    public record UpdateSpecialUserRequest(Integer id, String mail, String password, String ownedId, String modifiedAt){}
    public record NewReminderRequest( String header, String title, String message, String gyms, Boolean special, Boolean isReminder, String reminderDate, String createdAt, String modifiedAt){}
    public record VoteRequest(Integer userId,Integer votedId){}
    public record DownloadMobileDataRequest(Integer userId,Integer gymId){}

    public record ChangeApplicationMainData(String applicationLog){}


}
