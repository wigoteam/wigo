package com.wigo.models;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "votingModel")
public class VotingModel {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "voting_id_sequence",
            sequenceName = "voting_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "voting_id_sequence"
    )
    @Column(name = "id")
    private Integer id;
    private Integer votedId;
    private Integer userId;
    private String createdAt;
    private String modifiedAt;
}
