package com.wigo.repositories;



import com.wigo.models.ProductCategoryModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductCategoryRepository extends JpaRepository<ProductCategoryModel,Integer> {
    List<ProductCategoryModel> findByCategoryName(String categoryName);
}
