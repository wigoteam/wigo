package com.wigo.controllers;


import com.wigo.AppConstants;
import com.wigo.Helpers.RequestHolder;
import com.wigo.models.ErrorResponseType;
import com.wigo.models.ProductModel;
import com.wigo.models.ResponseModel;
import com.wigo.repositories.ProductRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController

public class ProductController {
    private final ProductRepository repository;


    public ProductController(ProductRepository repository) {
        this.repository = repository;
    }



    public ResponseModel getProductById(Integer id) {
        Optional<ProductModel> model = repository.findById(id);
        if (model.isPresent()) {
            response.setMessage("Exist");

            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setMessage("unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        return response;
    }

    public List<ProductModel> getAllProducts() {
        return repository.findAll();
    }
    public List<ProductModel> getGymProducts(Integer id) {
        return repository.findByGym(id);
    }


    ResponseModel response = new ResponseModel();







    public ResponseModel createNewProduct(RequestHolder.NewProductRequest request) {
        Optional<ProductModel> productModel = repository.findByNameAndGym(request.name(), request.gym());
        if (productModel.isEmpty()) {
            ProductModel product = new ProductModel();
            product.setAvailable(request.isAvailable());
            product.setCreatedAt(request.createdAt());
            product.setCategory(request.category());
            product.setDescription(request.description());
            product.setPrice(request.price());
            product.setImage(request.image());
            product.setModifiedAt(request.modifiedAt());
            product.setName(request.name());
            product.setSalePercent(request.salePercent());
            product.setGym(request.gym());
            product.setOnSale(request.isOnSale());
            repository.save(product);
            response.setMessage("product Added Successfully ");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(product);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setMessage("product already exist ");
            response.setErrorType(ErrorResponseType.DataAlreadyExist);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        return response;
    }


    /******************************************************************************************************************/







    /****UPDATE PRODUCT****/

    public ResponseModel updateProduct( RequestHolder.UpdateProductRequest request) {
        Optional<ProductModel> productModel = repository.findById(request.id());
        if (productModel.isPresent()) {
            ProductModel product=productModel.get();
            product.setAvailable(request.isAvailable());
            product.setCategory(request.category());
            product.setDescription(request.description());
            product.setPrice(request.price());
            product.setImage(request.image());
            product.setModifiedAt(request.modifiedAt());
            product.setName(request.name());
            product.setSalePercent(request.salePercent());
            product.setGym(request.gym());
            product.setOnSale(request.isOnSale());
                repository.save(product);
                response.setMessage("product updated Successfully ");
                response.setErrorType(ErrorResponseType.Nothing);
                response.setReturnedBoolean(true);
                response.setObject(product);
                response.setErrorCode("20000");
                response.setThereIsAnError(false);
                response.setReturnedInteger(null);
                response.setReturnedList(null);
                response.setReturnedString(null);
                response.setReturnedMultipartFile(null);
            }  else {
            response.setMessage("product unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }

        return response;
    }




    public ResponseModel deleteProduct(Integer id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
            response.setMessage("product deleted Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setMessage("product unfounded with this id");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        return response;
    }




}
