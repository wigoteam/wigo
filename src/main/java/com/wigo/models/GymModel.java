package com.wigo.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "gym")
public class GymModel {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "gym_id_sequence",
            sequenceName = "gym_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "gym_id_sequence"
    )

    @Column(name = "id")
    private    Integer id;
    private    String name;
    private    Double longitude;
    private    Double latitude;
    private    String workingTime;
    private    String description;
    private    String phone;
    private    Integer owner;
    @Column(length = 2048)
    private String mondayPlan;
    @Column(length = 2048)
    private String tuesdayPlan;
    @Column(length = 2048)
    private String wednesdayPlan;
    @Column(length = 2048)
    private String thursdayPlan;
    @Column(length = 2048)
    private String fridayPlan;
    @Column(length = 2048)
    private String saturdayPlan;
    @Column(length = 2048)
    private String sundayPlan;
    @Column(length = 2048)
    private    String products;
    @Column(length = 2048)
    private    String advantages;
    @Column(length = 2048)
    private    String coachs;
    private    String color;
    private    String image;
    @Column(length = 2048)
    private    String workoutTypes;
    @Column(length = 2048)
    private    String moreImages;
    private    String memberships;
    private    String createdAt;
    private    String modifiedAt;



}
