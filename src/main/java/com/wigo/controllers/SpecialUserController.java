package com.wigo.controllers;

import com.wigo.Helpers.RequestHolder;
import com.wigo.Utils;
import com.wigo.models.*;
import com.wigo.repositories.SpecialUserRepository;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController

public class SpecialUserController {
    private final SpecialUserRepository specialUserRepository;

    public SpecialUserController(SpecialUserRepository specialUserRepository) {
        this.specialUserRepository = specialUserRepository;
    }


    public ResponseModel getSpecialUserById( Integer id) {
        Optional<SpecialUserModel> model= specialUserRepository.findById(id);
        if(model.isPresent()){
            response.setMessage("Exist");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model.get());
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
        }else {
            response.setMessage("unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
        }
        response.setReturnedInteger(null);
        response.setReturnedList(null);
        response.setReturnedString(null);
        response.setReturnedMultipartFile(null);
        return response;
    }


    public ResponseModel loginSpecialUser( RequestHolder.LoginRequest request) {
        System.out.println(request);
        Optional<SpecialUserModel> profileModel = specialUserRepository.findByMail(request.email());
        if (profileModel.isPresent()) {
            if ((profileModel.get().getPassword().trim()).equals(request.password().trim())) {
                specialUserRepository.save(profileModel.get());
                response.setErrorType(ErrorResponseType.Nothing);
                response.setMessage(profileModel.get().getOwnedId());
                response.setErrorCode("20000");
                response.setThereIsAnError(false);
                response.setObject(profileModel.get());
            } else {
                response.setErrorType(ErrorResponseType.WrongPassword);
                response.setMessage("wrong password");
                response.setErrorCode("40000");
                response.setThereIsAnError(true);
                response.setObject(profileModel.get());
            }
        } else {
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setMessage("no user found with this information");
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setObject(null);
        }

        return response;


    }


    public ResponseModel preLogin(RequestHolder.PreLoginRequest request) {

        Optional<SpecialUserModel> profileModel = specialUserRepository.findByMail(request.email());
        if (profileModel.isPresent()) {
            response.setErrorType(ErrorResponseType.Nothing);
            response.setMessage("Valid");
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedString(profileModel.get().getRole().name());
            response.setObject(null);
        } else {
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setMessage("no user found with this information");
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setObject(null);
            response.setReturnedString(null);
        }
        return response;
    }



    public ResponseModel checkSpecialUserExistByEmail(String email) {
        Optional<SpecialUserModel> profileModel = specialUserRepository.findByMail(email);
        if (profileModel.isPresent()) {
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setMessage("true");
            response.setObject(profileModel.get());
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);

        } else {
            response.setErrorType(ErrorResponseType.Nothing);
            response.setMessage("false");
            response.setReturnedBoolean(false);
            response.setErrorCode("40000");
            response.setThereIsAnError(false);
            response.setObject(null);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);

        }
        return response;


    }




    public List<SpecialUserModel> getAllSpecialUsers() {
        return specialUserRepository.findAll();
    }



    ResponseModel response = new ResponseModel();






    public ResponseModel createSpecialUser(RequestHolder.NewSpecialUserRequest request) {
        if (!checkSpecialUserExistByEmail(request.mail()).getReturnedBoolean()) {
            SpecialUserModel profile = new SpecialUserModel();
            profile.setRole(request.role());
            profile.setPassword(request.password());
            profile.setMail(request.mail());
            profile.setOwnedId(request.ownedId());
            profile.setCreatedAt(LocalDate.now().toString());
            profile.setModifiedAt(LocalDate.now().toString());
            specialUserRepository.save(profile);
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(profile);
            response.setMessage("user created successfully");
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(profile.getId());
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setErrorType(ErrorResponseType.CorruptedData);
            response.setMessage("user already exist");
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        return response;
    }




    public ResponseModel updateSpecialUser( RequestHolder.UpdateSpecialUserRequest request) {

        Optional<SpecialUserModel> profileModel = specialUserRepository.findById(request.id());
        if (profileModel.isPresent()) {
            SpecialUserModel profile = profileModel.get();
            profile.setPassword(request.password());
            profile.setMail(request.mail());
            profile.setOwnedId(request.ownedId());
            profile.setModifiedAt(LocalDate.now().toString());
            //ban
            //message
            specialUserRepository.save(profile);
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(profile);
            response.setMessage("user updated successfully");
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(profile.getId());
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setMessage("user unfounded");
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        return response;
    }

    public ResponseModel deleteSpecialUser(Integer id) {
        if (specialUserRepository.existsById(id)) {
            specialUserRepository.deleteById(id);
            response.setMessage("user deleted Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setMessage("user unfounded with this id");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        return response;
    }


}
