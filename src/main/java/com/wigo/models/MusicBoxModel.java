package com.wigo.models;
import jakarta.persistence.*;


@Entity(name = "musicBox")
public class MusicBoxModel {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "music_box_id_sequence",
            sequenceName = "music_box_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "music_box_id_sequence"
    )
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "link")
    private String link;
    @Column(name = "upVote")
    private Integer upVote;
    private String createdAt;
    private String modifiedAt;
}
