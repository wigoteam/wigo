package com.wigo.models;

import jakarta.persistence.*;


@Entity(name = "achievement")
public class AchievementModel {
    @Id
    @SequenceGenerator(
            allocationSize = 1,
            name = "achievement_id_sequence",
            sequenceName = "achievement_id_sequence")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "achievement_id_sequence"
    )
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "image")
    private String image;
    @Column(name = "description")
    private String description;
    private String createdAt;
    private String modifiedAt;
}
