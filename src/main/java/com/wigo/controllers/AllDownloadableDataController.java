
package com.wigo.controllers;


import com.wigo.Helpers.RequestHolder;
import com.wigo.models.*;
import com.wigo.repositories.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AllDownloadableDataController {
    private final GymRepository gymRepository;
    private final CoachRepository coachRepository;
    private final DealsRepository dealsRepository;
    private final NotificationRepository notificationRepository;
    private final ReminderRepository reminderRepository;
    private final ProductRepository productRepository;
    private final ProductCategoryRepository productCategoryRepository;
    private final WorkoutTypeRepository workoutTypeRepository;





    ResponseModel response = new ResponseModel();




    public ResponseModel getAllMobileDaTa(RequestHolder.DownloadMobileDataRequest request) {

   Optional<GymModel> gymModel=gymRepository.findById(request.gymId());
   if (gymModel.isPresent()){
       List<GymModel> gyms=gymRepository.findAll();
       List<CoachModel> coachs=coachRepository.findByGym(request.gymId());
       List< DealsModel > deals=dealsRepository.findAll();
       List< NotificationModel > notifications=notificationRepository.findAll();
       List<ReminderModel> reminders=reminderRepository.findAll();
       List<ProductCategoryModel> productCategories=productCategoryRepository.findAll();
       List<ProductModel> products=productRepository.findByGym(request.gymId());
       List<WorkoutTypeModel> workoutTypes=workoutTypeRepository.findAll();
       AllMobileDownloadableDataModel allMobileDownloadableDataModel=new AllMobileDownloadableDataModel();
       allMobileDownloadableDataModel.setGyms(gyms);
       allMobileDownloadableDataModel.setDeals(deals);
       allMobileDownloadableDataModel.setCoachs(coachs);
       allMobileDownloadableDataModel.setNotifications(notifications);
       allMobileDownloadableDataModel.setProducts(products);
       allMobileDownloadableDataModel.setReminders(reminders);
       allMobileDownloadableDataModel.setWorkoutTypes(workoutTypes);
       allMobileDownloadableDataModel.setProductCategories(productCategories);
       response.setMessage("downloading");
       response.setErrorType(ErrorResponseType.Nothing);
       response.setReturnedBoolean(true);
       response.setObject(allMobileDownloadableDataModel);
       response.setErrorCode("20000");
       response.setThereIsAnError(false);
       response.setReturnedInteger(null);
       response.setReturnedList(null);
       response.setReturnedString(null);
       response.setReturnedMultipartFile(null);

   }else {
            response.setMessage("unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);


    }
        return response;
    }


}

