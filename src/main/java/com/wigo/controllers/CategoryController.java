
package com.wigo.controllers;


import com.wigo.AppConstants;
import com.wigo.Helpers.RequestHolder;
import com.wigo.models.ErrorResponseType;
import com.wigo.models.ProductCategoryModel;
import com.wigo.models.ResponseModel;
import com.wigo.repositories.ProductCategoryRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController

public class CategoryController {
    private final ProductCategoryRepository productCategoryRepository;


    public CategoryController(ProductCategoryRepository productCategoryRepository) {
        this.productCategoryRepository = productCategoryRepository;

    }



 ResponseModel response = new ResponseModel();


    public ResponseModel getProductCategoryById( Integer id) {
        Optional<ProductCategoryModel> model=productCategoryRepository.findById(id);
        if(model.isPresent()){
            response.setMessage("Exist");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(model);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }else {
            response.setMessage("unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setReturnedBoolean(false);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        return response;
    }


    public List<ProductCategoryModel> getAllProductCategories() {
        return productCategoryRepository.findAll();
    }



    public ResponseModel createProductCategory( RequestHolder.CreateProductCategoryRequest request )  {

       List<ProductCategoryModel> productCategoryModelList=productCategoryRepository.findByCategoryName(request.categoryName());
        if(productCategoryModelList.isEmpty()){
           ProductCategoryModel productCategoryModel = new ProductCategoryModel();
           productCategoryModel.setCategoryName(request.categoryName());
           productCategoryModel.setImage(request.image());
           productCategoryModel.setCreatedAt(request.createdAt());
           productCategoryModel.setModifiedAt(request.modifiedAt());
            productCategoryRepository.save(productCategoryModel);
            response.setMessage("category Added Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setErrorCode("20000");
            response.setObject(productCategoryModel);
            response.setReturnedBoolean(true);
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
            response.setThereIsAnError(false);
        }else {
            response.setMessage("category already exist");
            response.setErrorType(ErrorResponseType.DataAlreadyExist);
            response.setErrorCode("40000");
            response.setObject(null);
            response.setReturnedBoolean(false);
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
            response.setThereIsAnError(false);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Product Category created"));
        return response;
    }


    public ResponseModel updateProductCategory( RequestHolder.UpdateProductCategoryRequest request) {

        Optional<ProductCategoryModel> productCategoryModel = productCategoryRepository.findById(request.id());
        if (productCategoryModel.isPresent()){
            ProductCategoryModel category = productCategoryModel.get();
            category.setCategoryName(request.categoryName());
           category.setModifiedAt(request.modifiedAt());
           category.setImage(request.image());
            productCategoryRepository.save(category);
            response.setMessage("category information updated Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setErrorCode("20000");
            response.setObject(category);
            response.setReturnedBoolean(true);
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
            response.setThereIsAnError(false);
        }else {
            response.setMessage("category unfounded");
            response.setErrorType(ErrorResponseType.NoDataFound);
            response.setErrorCode("40000");
            response.setObject(null);
            response.setReturnedBoolean(false);
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
            response.setThereIsAnError(false);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Product Category Updated"));
        return response;
    }
    public ResponseModel deleteProductCategory(Integer id) {

        if (productCategoryRepository.existsById(id)) {
            productCategoryRepository.deleteById(id);
            response.setMessage("coach account deleted Successfully");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("20000");
            response.setThereIsAnError(false);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        } else {
            response.setMessage("coach unfounded with this id");
            response.setErrorType(ErrorResponseType.Nothing);
            response.setReturnedBoolean(true);
            response.setObject(null);
            response.setErrorCode("40000");
            response.setThereIsAnError(true);
            response.setReturnedInteger(null);
            response.setReturnedList(null);
            response.setReturnedString(null);
            response.setReturnedMultipartFile(null);
        }
        ApplicationMainDataCheckerController.changeApplicationMainData(new RequestHolder.ChangeApplicationMainData("Product Category Deleted"));
        return response;
    }
}

