package com.wigo.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Permission {

    ADMIN_READ("admin:read"),
    ADMIN_UPDATE("admin:update"),
    ADMIN_CREATE("admin:create"),
    ADMIN_DELETE("admin:delete"),
    GYM_READ("management:read"),
    GYM_UPDATE("management:update"),
    GYM_CREATE("management:create"),
    GYM_DELETE("management:delete")

    ;

    private final String permission;
}
