package com.wigo.repositories;

import com.wigo.models.DealsModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DealsRepository extends JpaRepository<DealsModel,Integer> {
    Optional<DealsModel> findByTitle(String title);
    List<DealsModel> findByGym(String gym);

}
