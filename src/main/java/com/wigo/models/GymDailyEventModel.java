package com.wigo.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GymDailyEventModel {
    private Integer type;
    private String name;
    private String image;
    private String startHour;
    private String endHour;
    private String description;
    private Integer coach;
    private Boolean eventIsSpecial;

    @Override
    public String toString() {
        return "{" +
                "\"type\":" + type +
                ",\" name\":\"" + name + "\"" +
                ",\" image\":\"" + image + "\"" +
                ",\" startHour\":\"" + startHour + "\"" +
                ",\" endHour\":\"" + endHour + "\"" +
                ",\" description\":\"" + description + "\"" +
                ",\" coach\":" + coach +
                ",\" eventIsSpecial\":" + eventIsSpecial +
                "}";
    }
}
