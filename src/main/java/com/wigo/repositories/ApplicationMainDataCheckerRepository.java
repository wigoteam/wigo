package com.wigo.repositories;

import com.wigo.models.ApplicationMainDataChecker;
import com.wigo.models.DealsModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ApplicationMainDataCheckerRepository extends JpaRepository<ApplicationMainDataChecker,Integer> {

}
